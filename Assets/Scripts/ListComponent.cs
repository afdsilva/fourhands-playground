﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using OniNM;

public class ListComponent : MonoBehaviour {
    public RectTransform CenterRect;
    public RectTransform listRect;
    public RectTransform fillTransform;
    public GameObject SkillItem;
    List<OniSkill> currentSkills = new List<OniSkill>();
    void Start() {
        //List<Skill> skills = new List<Skill>();
        //skills.Add(Skill.SkillRock);

        //SetSkillList(skills.ToArray());
    }

    public void SetSkillList(OniSkill[] skills) {
        currentSkills = new List<OniSkill>(skills);
        if (currentSkills == null) currentSkills = new List<OniSkill>();
        currentSkills.Clear();
        foreach (var item in skills) {
            GameObject go = Instantiate(SkillItem);
            go.transform.SetParent(transform);
            currentSkills.Add(item);
        }
    }

    void Update() {
        
    }
}
