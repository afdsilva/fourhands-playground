﻿using UnityEngine;
using System.Collections;
using System;

public abstract class ViewController : MonoBehaviour  {
    public GameMode ViewGameMode;
    public abstract void OpenView();
    public abstract void CloseView();
    public abstract void Unload();
    public abstract bool IsEnabled { get; protected set; }
    [Flags] public enum GameMode {
        NONE            = 0,
        LOADING         = 1,
        PAUSE_MENU      = 2,
        NOTIFICATION    = 4,
        EXPLORE         = 8,
        EVENT           = 16,
        TUTORIAL        = 32,
        ALL             = ~0
    }
    public static GameMode SetFlag(GameMode a, GameMode b) {
        return a | b;
    }
    public static GameMode UnsetFlag(GameMode a, GameMode b) {
        return a & (~b);
    }
    public static bool HasFlag(GameMode a, GameMode b) {
        if (a == b)
            return true;
        if (b == GameMode.NONE)
            return false;
        return (a & b) == b;
    }
    public static GameMode ToggleFlag(GameMode a, GameMode b) {
        return a ^ b;
    }
    public static GameMode ExceptFlag(GameMode a) {
        return UnsetFlag(GameMode.ALL, a);
    }
}
