﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.ResourceManagement.AsyncOperations;

public class RoomController : MonoBehaviour {
    public RoomData RoomData;
    public AssetReferenceRoomData RoomAssetReference;
    [SerializeField] private FurnitureManager furnitureManager;
    RoomManager roomManager { get { return RoomManager.Instance; } }
    public SpriteRenderer Background;
    
    public IEnumerator Initialize() {
        var operation = RoomAssetReference.LoadAssetAsync();
        operation.Completed += LoadRoomDataCompletedCallback;
        yield return operation;

        yield break;
    }
    private void LoadRoomDataCompletedCallback(AsyncOperationHandle<RoomData> obj) {
        RoomData = obj.Result;

        UpdateRoom(0);
    }
    public void SetRoomAsset(AssetReferenceRoomData assetReference) {
        cbOnExitRoom();
        RoomAssetReference = assetReference;
        furnitureManager = GetComponentInChildren<FurnitureManager>();
        //furnitureManager.SetData(data.Furnitures.ToArray(), furniturePrefab);
    }
    public void SetRoom(RoomData roomData) {
        RoomData = roomData;
        UpdateRoom(0);
    }

    public void unregisterCallbacks() {
        if (RoomData == null) return;
        RoomData.OnEnterRoom -= cbOnEnterRoom;
        RoomData.OnExitRoom -= cbOnExitRoom;
    }
    public void registerCallbacks() {
        if (RoomData == null) return;
        unregisterCallbacks();
        RoomData.OnEnterRoom += cbOnEnterRoom;
        RoomData.OnExitRoom += cbOnExitRoom;
    }
    void cbOnEnterRoom(RoomData obj) {
        gameObject.SetActive(true);
    }
    void cbOnExitRoom() {
        gameObject.SetActive(false);
    }
    public void UpdateRoom(float deltaTime) {
        if (RoomData == null) return;
        Background.sprite = RoomData.Background;
    }
    private void Awake() {
        Background = transform.Find("Background").GetComponent<SpriteRenderer>();
        if (Background == null) {
            GameObject backgroundGO = new GameObject("Background");
            Background = backgroundGO.AddComponent<SpriteRenderer>();
        }
        cbOnExitRoom();
    }
    void Start() {

    }

    // Update is called once per frame
    void Update() {

    }
}
