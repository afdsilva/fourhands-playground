﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
[CustomEditor(typeof(RoomController))]
public class RoomControllerEditor : Editor {
    RoomController roomController;
    private void OnEnable() {
        roomController = (RoomController)target;
        if (Application.isPlaying == false) {
            RoomData roomData = AssetDatabase.LoadAssetAtPath<RoomData>(AssetDatabase.GUIDToAssetPath(roomController.RoomAssetReference.guid));
            roomController.SetRoom(roomData);
        }

        roomController.registerCallbacks();

    }
    public override void OnInspectorGUI() {
        base.OnInspectorGUI();
        if (roomController.RoomData == null) GUI.enabled = false;
        if (GUILayout.Button("Enter Room") == true) {
            RoomManager.Instance.EnterRoom(roomController.RoomData);
        }
        GUI.enabled = true;
    }
}
