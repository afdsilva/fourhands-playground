﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicCharacterComponent : MonoBehaviour {
    //Animation Module
    public float Speed = 2f;
    Vector3 lastMoveDir;
    bool isIdle;
    protected Collider2D myCollider;

    //protected Vector3 startPosition, targetPosition;
    //protected bool moveAllowed;
    //protected System.Action cbOnCompleteMove;

    private void Awake() {
        myCollider = GetComponent<Collider2D>();
    }
    private void Update() {
        HandleMovement();
        HandleClick();
    }

    public void PlayAnimation(string animationName) {

    }

    const int UP = 1;
    const int DOWN = -1;
    const int LEFT = -1;
    const int RIGHT = 1;

    bool canMove(Vector3 dir, float distance) {
        //central
        int layerMask = LayerMask.GetMask("Obstacle");
        distance += myCollider.bounds.extents.x;
        RaycastHit2D hit = Physics2D.Raycast(transform.position, dir, distance, layerMask);
        if (hit.collider != null) return false;

        /**
        //top = 1, left = -1;
        hit = Physics2D.Raycast(getPosition(UP, LEFT), dir, distance, layerMask);
        if (hit.collider != null) return false;
        //top = 1, right = 1;
        hit = Physics2D.Raycast(getPosition(UP, RIGHT), dir, distance, layerMask);
        if (hit.collider != null) return false;
        hit = Physics2D.Raycast(getPosition(DOWN, LEFT), dir, distance, layerMask);
        if (hit.collider != null) return false;
        hit = Physics2D.Raycast(getPosition(DOWN, RIGHT), dir, distance, layerMask);
        if (hit.collider != null) return false;
        **/
        return true;
    }
    Vector3 getPosition(int upDown, int LeftRight) {
        float x = transform.position.x + myCollider.bounds.extents.x * LeftRight;
        float y = transform.position.y + myCollider.bounds.extents.y * upDown;
        return new Vector3(x, y, transform.position.z);
    }

    public bool TryMove(Vector3 baseMoveDir, float distance, bool allowMinDistMove = false) {
        Vector3 moveDir = baseMoveDir;
        bool movementAllowed = canMove(moveDir, distance);
        //Debug.Log("movementAllowed " + movementAllowed);
        if (movementAllowed == false) {
            //nao se move diagonalmente
            moveDir = new Vector3(baseMoveDir.x, 0f).normalized;
            movementAllowed = moveDir.x != 0f && canMove(moveDir, distance);
            if (movementAllowed == false) {
                //nao pode se mover horizontalmente
                moveDir = new Vector3(0f, baseMoveDir.y).normalized;
                movementAllowed = moveDir.y != 0 && canMove(moveDir, distance);
            }
        }
        if (movementAllowed == true) {
            lastMoveDir = moveDir;
            transform.position += moveDir * distance;
            return true;
        }
        return false;
    }
    public virtual void MoveTo(Vector3 position, System.Action cbOnCompleteMove = null, System.Action cbOnCancelMove = null) {
        //**code moved to TileCharacterComponent**//
        //moveAllowed = true;
        //startPosition = transform.position;
        //targetPosition = position;
        //this.cbOnCompleteMove = cbOnCompleteMove;

    }
    protected virtual void HandleMovement() {
        //**code moved to TileCharacterComponent**//
        //if (moveAllowed) {
        //    //Debug.Log("HandleMovement::moveAllowed::startPosition" + startPosition + "::targetPosition::" + targetPosition + "::Speed::" + Speed);
        //    transform.position = Vector3.MoveTowards(transform.position, targetPosition, Speed * Time.deltaTime);
        //    if (transform.position == targetPosition) {
        //        moveAllowed = false;
        //        cbOnCompleteMove?.Invoke();
        //        cbOnCompleteMove = null;
        //    }
        //}


        /**
        Vector3 axis = new Vector3(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        //Debug.Log(axis);
        isIdle = axis.x == 0 && axis.y == 0;
        if (isIdle == true) {
            //idle animation
        } else {
            lastMoveDir = axis.normalized;
            if (TryMove(lastMoveDir, Speed * Time.deltaTime) == true) {
                //walking animation
            } else {
                //idle animation
            }
        }
        **/
    }
    
    protected virtual void HandleClick() { }
}
