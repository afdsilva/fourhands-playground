﻿using UnityEngine;
//using System.Collections.Generic;

namespace FHSharedUtils {
    public abstract class FHSData : ScriptableObject, IActionable {
        public string Name;

        public abstract void DoAction(IActionable target, string action);
        public abstract string GetName();
    }

    public interface IActionable {
        string GetName();
        void DoAction(IActionable target, string action);
    }
    public interface ICollectable<T> {
        bool GetCollectable(string name, out T collectable);
        void AddCollectable(T collectable);
    }
}

