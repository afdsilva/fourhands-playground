﻿using UnityEngine;
using System.Collections;
using UnityEngine.Tilemaps;
using System.Collections.Generic;

namespace UnityEngine {
    public static class VectorExtensions {
        public static int ManhattanDistance(this Vector3Int a, Vector3Int b) {
            checked {
                return Mathf.Abs(a.x - b.x) + Mathf.Abs(a.y - b.y);
            }
        }
        public static int ManhattanDistance(this Vector2Int a, Vector2Int b) {
            checked {
                return Mathf.Abs(a.x - b.x) + Mathf.Abs(a.y - b.y);
            }
        }
        public static Vector3Int[] GetNeighbours(this Vector3Int pos, bool excludeDiagonals = false ) {
            List<Vector3Int> neighbours = new List<Vector3Int>();
            for (int x = -1; x <= 1; x++) {
                for (int y = -1; y <= 1; y++) {
                    if (x == 0 && y == 0) continue; //pula a si mesmo

                    if (excludeDiagonals && (Mathf.Abs(x + y) == 2 || x + y == 0)) continue;
                    int j = pos.x + x;
                    int l = pos.y + y;
                    Vector3Int neighbourTile = new Vector3Int(j, l, pos.z);
                    neighbours.Add(neighbourTile);
                }
            }
            return neighbours.ToArray();
        }
        public static Vector3Int[] GetNeighboursCutCorner(this Vector3Int pos, System.Func<Vector3Int, bool> func) {
            List<Vector3Int> neighbours = new List<Vector3Int>();
            for (int x = -1; x <= 1; x++) {
                for (int y = -1; y <= 1; y++) {
                    int j = pos.x + x;
                    int l = pos.y + y;
                    Vector3Int neighbourTile = new Vector3Int(j, l, pos.z);

                    if (Mathf.Abs(x + y) == 2 || x + y == 0) {
                        //-1 -1  ??  -1  1
                        // ??    00    ?? 
                        // 1 -1  ??   1  1
                        Vector3Int novacoisaX = new Vector3Int(pos.x + x, pos.y, pos.z);
                        Vector3Int novacoisaY = new Vector3Int(pos.x, pos.y + y, pos.z);
                        if (func(novacoisaX) == false || func(novacoisaY) == false) continue;
                    }
                    neighbours.Add(neighbourTile);
                }
            }
            return neighbours.ToArray();
        }
        public static Vector3Int[] GetDiagonals(this Vector3Int pos) {
            List<Vector3Int> neighbours = new List<Vector3Int>();
            for (int x = -1; x <= 1; x++) {
                for (int y = -1; y <= 1; y++) {
                    if (Mathf.Abs(x + y) == 2 || x + y == 0) {
                        int j = pos.x + x;
                        int l = pos.y + y;
                        Vector3Int neighbourTile = new Vector3Int(j, l, pos.z);
                        neighbours.Add(neighbourTile);
                    }
                }
            }
            return neighbours.ToArray();
        }
        public static Vector3 CellToWorldCenter(this Tilemap tilemap, Vector3Int pos) {
            Vector3 offset = new Vector3(0.5f, 0.5f, 0);
            return tilemap.CellToWorld(pos) + offset;
        }
        public static bool CheckValidTileCollider(this Tilemap tilemap, Vector3Int origin, Vector3Int target, LayerMask obstacleLayerMask) {
            Vector3 offset = new Vector3(0.5f, 0.5f, 0);
            Vector3 originPos = tilemap.CellToWorld(origin) + offset;
            Vector3 targetPos = tilemap.CellToWorld(target) + offset;
            Vector3 dir = (targetPos - originPos).normalized;
            float dist = Vector3.Distance(originPos, targetPos);
            //Debug.Log("CheckValidTilePath dir: " + dir + " dist: " + dist);
            RaycastHit2D hit = Physics2D.Raycast(originPos, dir, dist, obstacleLayerMask);
            if (hit.collider == null) {
                //Debug.DrawRay(originPos, dir * dist, Color.green, 10f);
                return true;
            } else {
                //Debug.DrawRay(originPos, dir * dist, Color.red, 10f);
                return false;
            }
        }
    }
}