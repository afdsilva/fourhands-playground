﻿using System.Collections;
using UnityEngine;

/// <summary>
/// Be aware this will not prevent a non singleton constructor
///   such as `T myT = new T();`
/// To prevent that, add `protected T () {}` to your singleton class.
/// 
/// As a note, this is made as MonoBehaviour because we need Coroutines.
/// </summary>
public abstract class Singleton<T> : MonoBehaviour where T : MonoBehaviour {
    private static T _instance;
    private static object _lock = new object();
    public static T Instance {
        get {
            if (applicationIsQuitting) {
                //Debug.LogWarning("[Singleton] Instance '" + typeof(T) + "' already destroyed on application quit." + " Won't create again - returning null.");
                if (Application.isPlaying == true)
                    return null;
            }

            lock (_lock) {
                if (_instance == null) {
                    _instance = (T)FindObjectOfType(typeof(T));

                    if (FindObjectsOfType(typeof(T)).Length > 1) {
                        //Debug.LogError("[Singleton] Something went really wrong " + " - there should never be more than 1 singleton!" + " Reopening the scene might fix it.");
                        return _instance;
                    }
                    
                    if (_instance == null) {
                        if (Application.isPlaying == false) {
                            //Debug.Log("Acessing script in Editor");
                            return null;
                        }
                        GameObject singleton = new GameObject();
                        _instance = singleton.AddComponent<T>();
                        singleton.name = "(singleton) " + typeof(T).ToString();
                        if (Application.isPlaying == true)
                            DontDestroyOnLoad(singleton);
                        //Debug.Log("[Singleton] An instance of " + typeof(T) + " is needed in the scene, so '" + singleton + "' was created with DontDestroyOnLoad.");
                    } else {
                        //Debug.Log("[Singleton] Using instance already created: " + _instance.gameObject.name);
                    }

                }

                return _instance;
            }
        }
    }
    protected static bool applicationIsQuitting = false;
    private static object _evenLock = new object();

    [field: System.NonSerialized] public static event System.Func<IEnumerator> Initialize;
    [field: System.NonSerialized] public static event System.Func<IEnumerator> Completed;

    /// <summary>
    /// When Unity quits, it destroys objects in a random order.
    /// In principle, a Singleton is only destroyed when application quits.
    /// If any script calls Instance after it have been destroyed, 
    ///   it will create a buggy ghost object that will stay on the Editor scene
    ///   even after stopping playing the Application. Really bad!
    /// So, this was made to be sure we're not creating that buggy ghost object.
    /// </summary>
    public virtual void Awake() {
        applicationIsQuitting = false;
        if (_instance == null) {
            _instance = this as T;
            if (Application.isPlaying == true)
                DontDestroyOnLoad(gameObject);
        } else {
            Destroy(gameObject);
            return;
        }

        SingletonAwake();
    }
    public abstract void SingletonAwake();
    protected IEnumerator Init() {
        if (init == true) yield break;
        if (Initialize != null) {
            foreach (System.Func<IEnumerator> item in Initialize.GetInvocationList()) {
                yield return item.Invoke();
            }
        }

        if (Completed != null) {
            foreach (System.Func<IEnumerator> item in Completed.GetInvocationList()) {
                yield return item.Invoke();
            }
        }
        init = true;

        yield break;
    }
    protected bool init = false;
    //public virtual void OnDestroy() {
    //    applicationIsQuitting = true;
    //}
    public virtual void OnApplicationQuit() {
        applicationIsQuitting = true;
    }
}