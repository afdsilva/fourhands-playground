﻿using UnityEngine;
using System.Collections;
using System;

namespace FourHands {
    public class Bind {
        public Bind(KeyCode trigger, Condition condition) {
            Trigger = trigger;
            Conditions = condition;
            MouseButtonTrigger = -1;
        }
        public Bind(KeyCode trigger) {
            Trigger = trigger;
            Conditions = Condition.Press;
            MouseButtonTrigger = -1;
        }
        public Bind(int mouseButtonTrigger, Condition condition) {
            Trigger = KeyCode.None;
            MouseButtonTrigger = mouseButtonTrigger;
            Conditions = condition;
        }
        public Bind(int mouseButtonTrigger) {
            Trigger = KeyCode.None;
            MouseButtonTrigger = mouseButtonTrigger;
            Conditions = Condition.Press;
        }
        public KeyCode Trigger { get; protected set; }
        public int MouseButtonTrigger { get; protected set; }
        public Condition Conditions { get; protected set; }

        [Flags]
        public enum Condition {
            None = 0,
            Press = 1,
            Release = 2,
            Hold = 4
        }
        public void SetFlag(Condition b) {
            Conditions = Conditions | b;
        }
        public void UnsetFlag(Condition b) {
            Conditions = Conditions & (~b);
        }
        public bool HasFlag(Condition b) {
            return (Conditions & b) == b;
        }
        public void ToggleFlag(Condition b) {
            Conditions = Conditions ^ b;
        }
        public bool Compare(Bind other) {
            if (Trigger == other.Trigger)
                return false;
            if (Conditions == other.Conditions)
                return false;
            return true;
        }
    }

}