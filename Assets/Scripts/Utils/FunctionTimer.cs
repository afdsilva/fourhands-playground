﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class FunctionTimer {
    public FunctionTimer(Action action, float timer, string timerName, GameObject gameObject) {
        this.action = action;
        this.timer = timer;
        this.gameObject = gameObject;
        this.timerName = timerName;
        isDestroyed = false;
    }

    string timerName;
    Action action;
    float timer;
    bool isDestroyed;
    GameObject gameObject;
    public void Update() {
        if (isDestroyed) return;
        timer -= Time.deltaTime;
        if (timer < 0) {
            action();
            DestroySelf();
        }
    }

    void DestroySelf() {
        isDestroyed = true;
        UnityEngine.Object.Destroy(gameObject);
        removeTimer(this);
    }

    static List<FunctionTimer> activeTimerList;
    static GameObject initGameObject;
    static void InitIfNeeded() {
        if (initGameObject == null) {
            initGameObject = new GameObject("functionTimer_InitGameObject");
            activeTimerList = new List<FunctionTimer>();
        }
    }
    public static FunctionTimer Create(Action action, float timer, string timerName = null) {
        InitIfNeeded();
        GameObject go = new GameObject("functionTimer", typeof(MonobehaviourHook));
        FunctionTimer functionTimer = new FunctionTimer(action, timer, timerName, go);
        go.GetComponent<MonobehaviourHook>().OnUpdate = functionTimer.Update;
        return null;
    }
    static void removeTimer(FunctionTimer functionTimer) {
        InitIfNeeded();
        activeTimerList.Remove(functionTimer);
    }
    public static void StopTimer(string timerName) {
        for (int i = 0; i < activeTimerList.Count; i++) {
            if (activeTimerList[i].timerName == timerName) {
                activeTimerList[i].DestroySelf();
                i--;
            }
        }
    }
    public class MonobehaviourHook : MonoBehaviour {
        public Action OnUpdate;
        private void Update() {
            OnUpdate?.Invoke();
        }
    }

}
