﻿using UnityEngine;
using UnityEngine.Tilemaps;
using UnityEngine.UI;
using System.Collections;
using OniNM;
using UnityEngine.EventSystems;
using System.Collections.Generic;

public class TileCharacterComponent : BasicCharacterComponent {

    public OniCharacter OniCharacter;
    public Tile CharacterTile;
    public SpriteRenderer baseSpriteRenderer;
    public SpriteRenderer Gfx;
    public Transform followTarget;
    public Slider ApCDTimer;
    public Slider HPBar;
    public Vector2 apCDOffset = new Vector2(20, 30);
    public Vector2 hpBarOffset = new Vector2(0, 65);
    public RPSIconController rPSIcon;
    private void Start() {
        if (ApCDTimer != null) {
            ApCDTimer.GetComponentInParent<Canvas>().worldCamera = Camera.main;
        }
    }
    public void SetCharacter(OniCharacter oniCharacter) {
        OniCharacter = oniCharacter;

        OniCharacter.OnSelection -= cbOnSelect;
        OniCharacter.OnSelection += cbOnSelect;
        OniCharacter.OnClearSelection -= cbOnClearSelection;
        OniCharacter.OnClearSelection += cbOnClearSelection;

        OniCharacter.OnUpdatePosition -= cbOnUpdatePosition;
        OniCharacter.OnUpdatePosition += cbOnUpdatePosition;
        cbOnUpdatePosition(OniCharacter.TilePosition);

        OniCharacter.OnAPTimerUpdate -= cbOnAPTimerUpdate;
        OniCharacter.OnAPTimerUpdate += cbOnAPTimerUpdate;
        cbOnAPTimerUpdate(OniCharacter.ApCDTimer, OniCharacter.ApCD);

        OniCharacter.TotalHP.OnHPChange -= cbOnHPChange;
        OniCharacter.TotalHP.OnHPChange += cbOnHPChange;
        cbOnHPChange(OniCharacter.TotalHP.Value, OniCharacter.TotalHP.MaxValue);

        OniCharacter.OnInSight -= cbOnBeingTargetted;
        OniCharacter.OnInSight += cbOnBeingTargetted;

        //OniCharacter.OnResolveStance -= rPSIcon.ShowStance;
        //OniCharacter.OnResolveStance += rPSIcon.ShowStance;
        //OniCharacter.OnSkillFail += cbOnSkillFail;
    }

    private void cbOnSkillFail(System.Exception obj) {
        Debug.Log("cbOnSkillFail " + obj.Message);
    }

    private void cbOnBeingTargetted(OniCharacter source, bool inSight) {
        //Debug.Log("cbOnBeingTargetted source " + source.Name + " OniCharacter " + OniCharacter.Name);
        if (source.IsPlayer == false) return;
        if (OniCharacter.IsPlayer) return;
        if (inSight == true) {
            show();
        } else {
            hide();
        }
    }
    private void cbOnAPTimerUpdate(float value, float maxValue) {
        if (ApCDTimer == null) return;
        ApCDTimer.maxValue = maxValue;
        ApCDTimer.value = value;
    }
    private void cbOnHPChange(int value, int maxValue) {
        if (HPBar == null) return;
        HPBar.maxValue = maxValue;
        HPBar.value = value;
    }
    private void cbOnUpdatePosition(Vector3 pos) {
        //Debug.Log("cbOnUpdatePosition: " + pos);
        //Vector3 screenPos = Camera.main.WorldToScreenPoint(new Vector3(pos.x + 0.4f, pos.y + 0.5f, pos.z));
        Vector3 screenPos = Camera.main.WorldToScreenPoint(pos);
        Vector2 movePos;
        
        if (HPBar != null) {
            Canvas parentCanvas = HPBar.GetComponentInParent<Canvas>();
            RectTransformUtility.ScreenPointToLocalPointInRectangle(parentCanvas.transform as RectTransform, screenPos, parentCanvas.worldCamera, out movePos);
            HPBar.transform.position = parentCanvas.transform.TransformPoint(movePos);
            HPBar.GetComponent<RectTransform>().anchoredPosition += hpBarOffset;
        }
        if (ApCDTimer != null) {
            Canvas parentCanvas = ApCDTimer.GetComponentInParent<Canvas>();
            RectTransformUtility.ScreenPointToLocalPointInRectangle(parentCanvas.transform as RectTransform, screenPos, parentCanvas.worldCamera, out movePos);
            //movePos.x = movePos.x + -10;
            ApCDTimer.transform.position = parentCanvas.transform.TransformPoint(movePos);
            ApCDTimer.GetComponent<RectTransform>().anchoredPosition += apCDOffset;
        }
        transform.position = pos;
    }

    Vector3 lastKnowTarget;

    void Update() {
        float time = Time.deltaTime;
        OniCharacter.Update(time);
        handleRPS(time);
    }

    void handleRPS(float time) {
        if (rpsEnabled == true) {
            rpsIconTimer -= time;
            if (rpsIconTimer <= 0) {
                rpsIconTimer = 0;
                rpsEnabled = false;
            }
        }
    }
    bool rpsEnabled;
    float rpsIconTimer;

    public void cbOnSelect(OniCharacter oniCharacter) {
        //Debug.Log(GetType().FullName + ".cbOnSelect = " + oniCharacter?.ToString());
        baseSpriteRenderer.color = Color.green;
    }
    public bool cbOnClearSelection(OniCharacter oniCharacter) {
        //Debug.Log(GetType().FullName + ".cbOnClearSelection = " + oniCharacter?.ToString());
        baseSpriteRenderer.color = Color.white;
        return false; //esse metodo nao pode alterar o estado do clearselection pq na pratica ele nao tem acesso ao item selecionado.
    }

    #region HANDLE_INPUT_COMMANDS
    public void SelectCommand() {
        OniCharacter.Select();
        //Debug.Log(OniCharacter.ActionPoints);
    }
    Vector3 startPosition;
    public void ActionCommand() {
        if (OniCharacterCollection.CheckSelectedPlayerCharacter(OniCharacter)) {
            startPosition = transform.position;
        }
    }
    public void ContinuousCommand() {
        if (OniCharacterCollection.CheckSelectedPlayerCharacter(OniCharacter)) {
            Vector3 worldMousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            worldMousePos.z = 0;
            transform.position = worldMousePos;
            OniNode[] fakePath;
            if (OniCharacter.FakePathPosition(worldMousePos, out fakePath) == true) {
                GridManagerController.Instance.SelectPath(fakePath, OniCharacter.ActionPoints);
            }
        }
    }
    public void EndCommand() {
        if (OniCharacterCollection.CheckSelectedPlayerCharacter(OniCharacter)) {
            transform.position = startPosition;
            Vector3 worldMousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            worldMousePos.z = 0;
            OniCharacter.Path(worldMousePos,
                (System.Exception e) => { Debug.Log(e.Message); },
                (System.Exception e) => { Debug.Log(e.Message); }
            );
            GridManagerController.Instance.ClearSelection();
        }
    }
    #endregion

    public void show() {
        baseSpriteRenderer.gameObject.SetActive(true);
        Gfx.gameObject.SetActive(true);
        //ApCDTimer.gameObject.SetActive(true);
        ToggleHP(true, true);
        ToggleApCD(true, true);
    }
    public void hide() {
        baseSpriteRenderer.gameObject.SetActive(false);
        Gfx.gameObject.SetActive(false);
        //ApCDTimer.gameObject.SetActive(false);
        ToggleHP(true, false);
        ToggleApCD(true, false);
    }
    public void ToggleHP(bool forceStatus = false, bool status = false) {
        bool newStatus = forceStatus == true ? status : !HPBar.gameObject.activeSelf;
        HPBar.gameObject.SetActive(newStatus);
    }
    public void ToggleApCD(bool forceStatus = false, bool status = false) {
        bool newStatus = forceStatus == true ? status : !ApCDTimer.gameObject.activeSelf;
        ApCDTimer.gameObject.SetActive(newStatus);
    }

}
