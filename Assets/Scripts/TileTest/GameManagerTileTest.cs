﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using OniNM;
using UnityEngine.AddressableAssets;

public class GameManagerTileTest : MonoBehaviour {

    public StorageSlotManager SlotStorageManager;
    //public 

    Vector3Int lastKnowHighlightCell;
    Vector3Int lastKnowSelectedCell;

    OniPathfinding oniPathfinding;

    void Start() {
        //OniCharacterCollection.Instance.InitCharacters();

    }

    void Update() {
        HandleMouse();
    }

    Collider2D lastKnowInteractable;
    bool actionTriggered;
    void HandleMouse() {
        if (actionTriggered == true) {
            lastKnowInteractable?.SendMessage("ContinuousCommand", SendMessageOptions.DontRequireReceiver);
            if (Input.GetMouseButtonUp(1)) {
                lastKnowInteractable?.SendMessage("EndCommand", SendMessageOptions.DontRequireReceiver);
                lastKnowInteractable = null;
                actionTriggered = false;
            }
        } else {
            Vector3 worldMousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            worldMousePos.z = 0;
            RaycastHit2D[] hits = Physics2D.RaycastAll(worldMousePos, Vector2.zero);
            Collider2D top;
            if (getColliderOnTop(hits, out top) == true) {

                if (Input.GetMouseButtonDown(0) == true) {
                    top.SendMessage("SelectCommand", SendMessageOptions.DontRequireReceiver);
                }
                if (Input.GetMouseButtonDown(1) == true) {
                    actionTriggered = true;
                    top.SendMessage("ActionCommand", SendMessageOptions.DontRequireReceiver);
                }
                lastKnowInteractable = top;
            }
        }
    }

    #region COLLIDER_ON_TOP
    bool getColliderOnTop(RaycastHit2D[] hits, out Collider2D top) {
        top = null;
        if (hits == null)
            return false;
        foreach (var hitInfo in hits) {
            if (hitInfo.collider.CompareTag("Interactable") == false) continue;
            top = isColliderOnTop(top, hitInfo.collider) == true ? hitInfo.collider : top;
        }

        return (top != null);
    }
    /// <summary>
    /// Checa dois colliders para verificar qual esta no topo
    /// </summary>
    /// <param name="current"></param>
    /// <param name="other"></param>
    /// <returns>retorna falso se o collider "current" esta no topo, retorna verdadeiro se o collider "other" esta no topo</returns>
    bool isColliderOnTop(Collider2D current, Collider2D other) {
        if (other == null)
            return false;
        if (current == null)
            return true;
        int weightCurrent = getLayerWeight(LayerMask.LayerToName(current.gameObject.layer));
        int weightOther = getLayerWeight(LayerMask.LayerToName(other.gameObject.layer));
        if (weightCurrent == weightOther) //mesmo layer
            return isSpriteRenderOnTop(current, other);
        return weightCurrent < weightOther;
    }
    /// <summary>
    /// Checa dois SpriteRenderers para verificar qual está no topo
    /// </summary>
    /// <param name="current"></param>
    /// <param name="other"></param>
    /// <returns>Retorna verdadeiro caso Collider "other" esteja no topo, retorna falso caso o "current" esteja no topo</returns>
    bool isSpriteRenderOnTop(Collider2D current, Collider2D other) {
        
        SpriteRenderer currentSpriteRenderer = current.transform.Find("Gfx").GetComponent<SpriteRenderer>();
        if (currentSpriteRenderer == null) throw new System.NullReferenceException("currentSpriteRenderer NULL. " + current.name);
        SpriteRenderer otherSpriteRenderer = other.transform.Find("Gfx").GetComponent<SpriteRenderer>();
        if (otherSpriteRenderer == null) throw new System.NullReferenceException("otherSpriteRenderer NULL. " + other.name);
        if (currentSpriteRenderer == otherSpriteRenderer) return false; //testando mesmo render
        if (getSortingLayerWeight(currentSpriteRenderer.sortingLayerName) >= getSortingLayerWeight(otherSpriteRenderer.sortingLayerName)) return false;
        if (currentSpriteRenderer.sortingOrder >= otherSpriteRenderer.sortingOrder) return false;

        return true;
    }
    int getSortingLayerWeight(string sortingLayerName) {
        switch (sortingLayerName) {
            case "Background":
                return 0;
            case "Default":
                return 1;
            case "Foreground":
                return 2;
            case "Cinematic":
                return 3;
            case "UI":
                return 4;
            default:
                return -1;
        }
    }
    int getLayerWeight(string layerName) {
        switch (layerName) {
            case "Character":
                return 0;
            case "Obstacle":
                return 1;
            default:
                return -1;
        }
    }
    #endregion

    void selectTile(Vector3Int pos) {
        /**
        Debug.Log("selectTile " + pos);
        if (validTile(pos) == false) return;

        clearTile(lastKnowSelectedCell);
        if (lastKnowSelectedCell == pos) {
            //deseleciona
            lastKnowSelectedCell = new Vector3Int(-100, -100, -100);
        } else {
            //seleciona
            SelectionTilemap.SetTile(pos, SelectedTile);
            lastKnowSelectedCell = pos;
            Vector3 nPos = SelectionTilemap.CellToWorld(pos);
            //CharacterTest.GetComponent<BasicCharacterComponent>().MoveTo(nPos + offset, () => {
            //    Debug.Log(nPos);
            //});
            //CharacterTest.transform.localPosition = nPos + offset;
        }
        **/
    }
    //void highlightTile(Vector3Int pos) {
    //    if (validTile(pos) == false || lastKnowHighlightCell == pos) return;
    //    if (lastKnowHighlightCell != lastKnowSelectedCell) clearTile(lastKnowHighlightCell);
    //    if (lastKnowSelectedCell == pos) return;
    //    OniGridManager.Instance.SelectionTilemap.SetTile(pos, HighlightTile);
    //    lastKnowHighlightCell = pos;
    //}
    //void clearTile(Vector3Int pos) {
    //    if (validTile(pos) == false) return;
    //    OniGridManager.Instance.SelectionTilemap.SetTile(pos, SelectionTile);
    //}
    //bool validTile(Vector3Int pos) {
    //    return (OniGridManager.Instance.SelectionTilemap.GetTile(pos) != null);
    //}

    [SerializeField] string airElementName = "Air";

    void CreateOni(string elementName) {
        throw new System.NotImplementedException();
        //try {
        //    Element element;
        //    if (OniElementManager.GetElement(airElementName, out element) == false) throw new System.NullReferenceException(elementName + " not found.");
        //    Oni oni;
        //    //if (Oni.Create(element, out oni) == false) throw new System.NullReferenceException("can't create new " + elementName + " oni");
        //    OniCharacter selectedCharacter;
        //    if (OniCharacterManager.GetSelectedCharacter(out selectedCharacter) == false) throw new System.Exception("no character selected.");
        //    StorageSlot storageSlot;
        //    if (selectedCharacter.GetEmptyStorageSlot(out storageSlot) == false) throw new System.Exception("no empty slots available.");
        //    storageSlot.Equip(oni);
        //} catch (System.NullReferenceException e) {
        //    Debug.LogError(e.Message);
        //} catch (System.Exception e) {
        //    Debug.LogWarning(e.Message);
        //}

    }

    public void CreateAirOni() {
        CreateOni(airElementName);
    }

    [SerializeField]string waterElementName = "Water";
    public void CreateWaterOni() {
        CreateOni(waterElementName);
    }

    [SerializeField] string fireElementName = "Fire";
    public void CreateFireOni() {
        CreateOni(fireElementName);
    }

    [SerializeField] string earthElementName = "Earth";
    public void CreateEarthOni() {
        CreateOni(earthElementName);
    }
}
