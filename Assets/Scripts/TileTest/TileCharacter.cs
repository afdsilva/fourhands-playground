﻿using UnityEngine;
using System.Collections;
using UnityEngine.Tilemaps;
[CreateAssetMenu(fileName = "TileCharacter", menuName = "CustomTiles/TileCharacter")]
public class TileCharacter : Tile {
    public GameObject m_Prefab;
    public override void GetTileData(Vector3Int location, ITilemap tilemap, ref TileData tileData) {
        //tileData.sprite = m_Sprite;
        tileData.gameObject = m_Prefab;
    }
    public static event System.Func<Vector3Int, OniNM.OniCharacter> OnRequestCharacterInfo;
    public bool GetOniCharacter(Vector3Int pos, out OniNM.OniCharacter oniCharacter) {
        if (OnRequestCharacterInfo == null) throw new System.Exception("No OnRequestCharacterInfo callback registered.");
        oniCharacter = null;
        foreach (System.Func<Vector3Int, OniNM.OniCharacter> item in OnRequestCharacterInfo.GetInvocationList()) {
            oniCharacter = item.Invoke(pos);
            if (oniCharacter == null) continue;
            return true;
        }
        return false;
    }
}
