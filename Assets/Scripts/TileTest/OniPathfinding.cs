﻿using UnityEngine;
using UnityEngine.Tilemaps;
using System.Collections.Generic;
using System;

namespace OniNM {
    public class OniPathfinding {
        public static event System.Action<OniNode[]> OnPathGenerated;

        protected OniPathfinding() { }
        public OniPathfinding(System.Func<Vector3Int, bool> cbCheckValid, System.Func<Vector3Int, bool, float> cbCheckCost) {
            checkValidFunc = cbCheckValid;
            checkCostFunc = cbCheckCost;
        }
        System.Func<Vector3Int, bool> checkValidFunc;
        System.Func<Vector3Int, bool, float> checkCostFunc;
        public bool Search(Vector3Int start, Vector3Int target, out Queue<OniNode> path) {
            OniNode[] nodes;
            if (Search(start, target, out nodes, true) == false) {
                path = new Queue<OniNode>();
                return false;
            }
            path = new Queue<OniNode>(nodes);
            return true;
        }
        public bool Search(Vector3Int start, Vector3Int target, out OniNode[] path, bool useQueueOrder = false) {
            path = null;
            if (checkValidFunc(target) == false) return false;
            List<OniNode> closedList = new List<OniNode>();
            List<OniNode> openList = new List<OniNode>();
            //OniNode targetNode = new OniNode(0, heuristicCostEstimate(start, target), null, target);
            OniNode startNode = new OniNode(0, 0, null, start);
            openList.Add(startNode);

            OniNode currentNode = null;
            while (openList.Count > 0) {
                currentNode = lowestFScore(openList.ToArray());
                if (currentNode.TilePos == target) {
                    path = buildPath(currentNode, useQueueOrder);
                    return true;
                }
                openList.Remove(currentNode);
                closedList.Add(currentNode);

                OniNode[] neighbours = getNeighborNodes(currentNode);
                for (int i = 0; i < neighbours.Length; i++) {
                    OniNode neighbor = neighbours[i];
                    if (closedList.Find(n => n.TilePos == neighbor.TilePos) != null) continue;
                    neighbor.H = heuristicCostEstimate(neighbor.TilePos, target);

                    //int tentative_gScore = currentNode.g + 1;
                    OniNode neighborInOpen = openList.Find(n => n.TilePos == neighbor.TilePos);
                    if (neighborInOpen == null) openList.Add(neighbor);
                    else {
                        if (neighbor.G < neighborInOpen.G) {
                            neighborInOpen.G = neighbor.G;
                            neighborInOpen.Parent = neighbor.Parent;
                        }
                    }
                }
            }
            return false;
        }
        OniNode[] buildPath(OniNode root, bool useQueueOrder = false) {
            List<OniNode> pathList = new List<OniNode>();
            OniNode current = root;
            do {
                pathList.Add(current);
                //Debug.Log("buildPath " + current.tilePos);
                current = current.Parent;
                //if (current.parent == null) break;
            } while (current != null);
            if (useQueueOrder) pathList.Reverse();
            OnPathGenerated?.Invoke(pathList.ToArray());
            return pathList.ToArray();
        }
        int heuristicCostEstimate(Vector3Int start, Vector3Int end) {
            int weight = start.ManhattanDistance(end);
            return weight;
        }
        OniNode lowestFScore(OniNode[] oniNodes) {
            OniNode lowest = oniNodes[0];
            for (int i = 0; i < oniNodes.Length; i++) {
                if (oniNodes[i].f < lowest.f) lowest = oniNodes[i];
            }
            return lowest;
        }

        OniNode[] getNeighborNodes(OniNode node) {
            List<OniNode> neighbours = new List<OniNode>();
            List<Vector3Int> neighboursPos = new List<Vector3Int>();
            for (int x = -1; x <= 1; x++) {
                for (int y = -1; y <= 1; y++) {
                    if (x == 0 && y == 0) continue;
                    int j = node.TilePos.x + x;
                    int l = node.TilePos.y + y;
                    Vector3Int neighbourTile = new Vector3Int(j, l, node.TilePos.z);
                    if (checkValidFunc(neighbourTile) == false) continue; //verifica validade do vizinho
                    bool isDiagonal = false;
                    //checa se eh possivel passar pela diagonal
                    if (Mathf.Abs(x + y) == 2 || x + y == 0) {
                        isDiagonal = true;
                        //-1 -1  ??  -1  1
                        // ??    00    ?? 
                        // 1 -1  ??   1  1
                        Vector3Int novacoisaX = new Vector3Int(node.TilePos.x + x, node.TilePos.y, node.TilePos.z);
                        Vector3Int novacoisaY = new Vector3Int(node.TilePos.x, node.TilePos.y + y, node.TilePos.z);
                        if (checkValidFunc(novacoisaX) == false || checkValidFunc(novacoisaY) == false) continue;
                    }
                    float g = node.G + checkCostFunc(neighbourTile, isDiagonal);
                    OniNode neighbor = new OniNode(g, float.MaxValue, node, neighbourTile);
                    neighbours.Add(neighbor);
                }
            }
            return neighbours.ToArray();
        }

    }
    
    public class OniNode {

        public float G { get; set; }
        public float H { get; set; }
        public OniNode Parent { get; set; }
        public Vector3Int TilePos { get; set; }
        protected OniNode() {
            Parent = null;
        }
        public OniNode(float g, float h, OniNode parent, Vector3Int tilePos) {
            G = g; // Steps from A to this
            H = h; // Steps from this to B
            Parent = parent;
            TilePos = tilePos;
        }
        public float f { get { return G + H; } }

        public bool CheckPos(OniNode other) {
            return (TilePos == other.TilePos);
        }
        public override string ToString() {
            return "(" + TilePos.x + ", " + TilePos.y + ", " + TilePos.z + ") g: " + G + " h: " + H + " parent: " + (Parent != null ? "(" + Parent.TilePos.x + ", " + Parent.TilePos.y + ", " + Parent.TilePos.z + ")" : "NULL");
        }
    }

}
