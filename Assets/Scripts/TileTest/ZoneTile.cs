﻿using UnityEngine;
using System.Collections;
using UnityEngine.Tilemaps;
using OniNM;

[CreateAssetMenu(fileName = "ZoneTile", menuName = "CustomTiles/ZoneTile")]
public class ZoneTile : Tile {
    public OniElementPrototype Element;
    public bool Walkable = true;
    public float Cost = 1;
}
