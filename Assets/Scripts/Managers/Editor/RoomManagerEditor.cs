﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.AddressableAssets;
using UnityEditor.AddressableAssets.Settings;

[CustomEditor(typeof(RoomManager))]
public class RoomManagerEditor : Editor {
    RoomManager RoomManager;
    public GameObject RoomPrefab;
    static Dictionary<AssetReferenceRoomData, GameObject> roomDataLoadedFromAddressables = new Dictionary<AssetReferenceRoomData, GameObject>();



    private void OnEnable() {
        RoomManager = (RoomManager)target;
    }


    public override void OnInspectorGUI() {
        base.OnInspectorGUI();
        RoomPrefab = EditorGUILayout.ObjectField("Room Prefab", RoomPrefab, typeof(GameObject), false) as GameObject;

        if (GUILayout.Button("Generate Rooms") == true) {
            if (EditorUtility.DisplayDialog("Generating Rooms", "Remove old Rooms?", "Yes", "No")) {
                GenerateRooms(true);
            } else {
                GenerateRooms();
            }
        }
        if (GUILayout.Button("Build Database") == true) {
            //RoomManager.LoadAllRooms();
        }
    }

    void GenerateRooms(bool forceRemove = false) {
        throw new System.NotImplementedException("obsolete");
        //var aaSettings = AddressableAssetSettingsDefaultObject.GetSettings(true);
        //List<AddressableAssetEntry> assetsList = new List<AddressableAssetEntry>(2048);
        
        ////aaSettings.GetAllAssets(assetsList, null, filter);

        //foreach (var item in assetsList) {
        //    CreateController(item, RoomManager.transform, forceRemove);
        //}

    }
    public GameObject CreateController(AddressableAssetEntry assetEntry, Transform parent, bool forceRemove = false) {
        /**
        if (RoomPrefab == null) throw new System.NullReferenceException("Room Prefab not setted.");
        RoomData roomData = AssetDatabase.LoadAssetAtPath<RoomData>(AssetDatabase.GUIDToAssetPath(assetEntry.guid));
        AssetReferenceRoomData assetReference;
        GameObject go;
        RoomController controller;// = go.GetComponent<RoomController>();
        if (GetRoomReference(assetEntry.guid, out assetReference) == true) {

            if (forceRemove == true) {
                if (RemoveRoomReference(assetEntry.guid) == true) {
                    controller = assetReference.GameObjectController.GetComponent<RoomController>();
                    if (controller != null) controller.unregisterCallbacks();
                    DestroyImmediate(assetReference.GameObjectController);
                    assetReference.GameObjectController = null;
                    controller = null;
                }
                go = NewController(assetReference, roomData, parent);
            } else {
                go = assetReference.GameObjectController;
            }
        } else {
            assetReference = new AssetReferenceRoomData(assetEntry.guid);
            go = NewController(assetReference, roomData, parent);

        }
        controller = go.GetComponent<RoomController>();
        controller.SetRoomAsset(assetReference);
        return go;
        /**/
        return null;
    }
    public GameObject NewController(AssetReferenceRoomData assetReference, RoomData roomData, Transform parent) {
        /**
        GameObject go = Instantiate(RoomPrefab, parent);
        //RoomController controller = go.GetComponent<RoomController>();
        go.name = roomData.name;
        assetReference.GameObjectController = go;
        AddRoomReference(assetReference);
        return go;
        **/
        return null;
    }
    bool filter(AddressableAssetEntry entry) {
        return entry.labels.Contains("RoomData");
        //return false;
    }

    public bool GetRoomReference(string guid, out AssetReferenceRoomData reference) {
        throw new System.NotImplementedException("GetRoomReference obsolete");
        //reference = roomDataLoadedFromAddressables.ContainsKey(r => r.guid == guid);
        //return (reference != null);
    }
    public bool RemoveRoomReference(string guid) {
        throw new System.NotImplementedException("RemoveRoomReference obsolete");
        //AssetReferenceRoomData reference;
        //if (GetRoomReference(guid, out reference) == false)
        //    return false;
        //return roomDataLoadedFromAddressables.Remove(reference);
    }
    public void AddRoomReference(AssetReferenceRoomData assetReferenceRoomData) {
        throw new System.NotImplementedException("AddRoomReference obsolete");

        //roomDataLoadedFromAddressables.Add(assetReferenceRoomData);
    }
}
