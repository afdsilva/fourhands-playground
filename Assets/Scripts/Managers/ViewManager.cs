﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Collections;

public class ViewManager : Singleton<ViewManager> {
    public override void SingletonAwake() {
    }
    //public override IEnumerator SingletonStart() {
    //    if (viewManagerStack == null)
    //        viewManagerStack = new Stack<ViewController>();
    //    else
    //        viewManagerStack.Clear();
    //    yield break;
    //}

    Stack<ViewController> viewManagerStack = new Stack<ViewController>();
    static ViewController baseViewController;
    public static ViewController CurrentView {
        get {
            try {
                return Instance.viewManagerStack.Peek();
            } catch {
                return baseViewController;
            }
        }
    }
    public static ViewController.GameMode CurrentGameMode {
        get {
            try {
                return CurrentView.ViewGameMode;
            } catch {
                return ViewController.GameMode.NONE;
            }
        }
    }

    public static bool ToggleView(ViewController view) {
        if (OpenView(view) == false) {
            return CloseView(view);
        }
        return true;
    }
    public static bool OpenView(ViewController view) {
        try {
            if (Instance.viewManagerStack.Count > 0 && Instance.viewManagerStack.Peek() == view)
                throw new Exception("OpenView::View already opened.");
            if (view.ViewGameMode == ViewController.GameMode.EXPLORE) {
                baseViewController = view;
            } else {
                Instance.viewManagerStack.Push(view);
            }
            view.OpenView();
            return true;
        } catch (Exception e) {
            Debug.LogWarning(e.Message);
            return false;
        }
    }
    public static bool CloseView(ViewController view) {
        try {
            if (view == null)
                throw new Exception("CloseView::NULL is not a valid view.");
            if (Instance.viewManagerStack.Peek() != view)
                throw new Exception("CloseView::View not on top.");
            if (view.ViewGameMode != ViewController.GameMode.EXPLORE) {
                Instance.viewManagerStack.Pop();
            }

            view.CloseView();
            return true;
        } catch (Exception e) {
            Debug.LogWarning(e.Message);
            return false;
        }
    }
    public static void CloseTopView() {
        try {
            ViewController topView = Instance.viewManagerStack.Pop();
            topView.CloseView();
        } catch {

        }
    }
}