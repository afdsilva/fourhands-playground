﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using TMPro;

public class RoomManager : Singleton<RoomManager> {
    public override void SingletonAwake() {
        LoadingManager.LoadRequest -= cbLoadRequest;
        LoadingManager.LoadRequest += cbLoadRequest;
    }

    public AssetReference RoomPrefab;
    [SerializeField] private RoomData currentRoom;
    //public TextMeshProUGUI loadedRoomsText;
    Dictionary<RoomData, GameObject> loadedRooms = new Dictionary<RoomData, GameObject>();

    public IEnumerator cbLoadRequest() {
        //Debug.Log(GetType().FullName + "::cbLoadRequest");
        var op = Addressables.LoadAssetsAsync<RoomData>("RoomData", RoomDataLoaded);
        op.Completed += roomsLoadedCompleted;
        //op.PercentComplete
        yield return op;

        //var op2 = RoomPrefab.LoadAssetAsync<GameObject>();
    }

    void ClearRooms() {
        loadedRooms.Clear();
        for (int i = 0; i < transform.childCount; i++) {
            Transform t = transform.GetChild(i);
            Destroy(t.gameObject);
        }
    }

    void roomsLoadedCompleted(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle<IList<RoomData>> obj) {
        //string showLoadedRooms = "";
        //foreach (var item in loadedRooms) {
        //    showLoadedRooms += item.Key.name + "\n";
        //}
        //loadedRoomsText.text = showLoadedRooms;
        Debug.Log(obj.Result.Count + " rooms loaded.");
    }
    void RoomDataLoaded(RoomData data) {

        loadedRooms.Add(data, null);
        //Cria o gameobject
        //preciso do prefab ja carregado

        //Debug.Log("Loading " + data.name);
    }
    void Update() {
        
    }
    public void EnterRoom(RoomData room) {
        if (room == null) return;
        ExitRoom();
        room.EnterRoom();
        currentRoom = room;
    }
    public void ExitRoom() {
        if (currentRoom != null) {
            currentRoom.ExitRoom();
        }
        currentRoom = null;

    }

    //public GameObject CreateRoomGameObject(RoomData data, Transform parent) {
    //    GameObject go = Instantiate(RoomPrefab, parent);
    //    RoomController controller = go.GetComponent<RoomController>();
    //    go.name = data.name;
    //    controller.SetRoomData(data, FurniturePrefab, this);
    //    return go;
    //}

    //[Serializable]
    //public class RoomDataGameObjectItem {
    //    public RoomDataGameObjectItem(RoomData data, GameObject go = null) {
    //        Data = data;
    //        GameObject = go;
    //    }
    //    public RoomData Data;
    //    public GameObject GameObject;
    //}
}
