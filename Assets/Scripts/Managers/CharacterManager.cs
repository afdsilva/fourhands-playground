﻿using FHSharedUtils;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;

public class CharacterManager : Singleton<CharacterManager>, ICollectable<CharacterData> {
    public override void SingletonAwake() {
        //LoadAssetManager.ModuleLoad += Init;
        LoadingManager.LoadRequest -= cbLoadRequest;
        LoadingManager.LoadRequest += cbLoadRequest;
    }

    [SerializeField] List<CharacterData> loadedCharacters = new List<CharacterData>();
    public IEnumerator cbLoadRequest() {
        //Debug.Log(GetType().FullName + "::cbLoadRequest");
        var op = Addressables.LoadAssetsAsync<CharacterData>("CharacterData", AddCollectable);
        op.Completed += charactersLoadedCompleted;
        //op.PercentComplete
        yield return op;
    }

    void charactersLoadedCompleted(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle<IList<CharacterData>> obj) {
        Debug.Log(obj.Result.Count + " characters loaded.");
    }

    public bool GetCollectable(string name, out CharacterData collectable) {
        collectable = loadedCharacters.Find(c => c.GetName() == name);
        return (collectable != null);
    }

    public void AddCollectable(CharacterData collectable) {

        if (loadedCharacters.Contains(collectable) == true) return;
        Debug.Log("AddCollectable " + collectable.GetName());
        loadedCharacters.Add(collectable);
    }
}
