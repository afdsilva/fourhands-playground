﻿using System;
using UnityEngine;
using System.Collections.Generic;

public class FurnitureManager : MonoBehaviour {
    public List<FurnitureDataGameObjectItem> FurnitureDataList = new List<FurnitureDataGameObjectItem>();
    public void SetData(FurnitureData[] furnitures, GameObject furniturePrefab) {
        FurnitureDataList.Clear();
        for (int i = 0; i < furnitures.Length; i++) {
            GameObject go = CreateFurnitureGameObject(furnitures[i], furniturePrefab);
            FurnitureDataList.Add(new FurnitureDataGameObjectItem(furnitures[i], go));
            //FurnitureDataList.Add(furnitures[i]);
        }
    }
    GameObject CreateFurnitureGameObject(FurnitureData data, GameObject furniturePrefab) {
        GameObject go = null;
        return go;
    }
    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {

    }

    [Serializable]
    public class FurnitureDataGameObjectItem {
        public FurnitureDataGameObjectItem(FurnitureData data, GameObject go = null) {
            Data = data;
            GameObject = go;
        }

        public FurnitureData Data;
        public GameObject GameObject;
    }
}
