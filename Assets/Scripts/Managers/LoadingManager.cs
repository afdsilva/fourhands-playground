﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;

public class LoadingManager : Singleton<LoadingManager> {
    public override void SingletonAwake() {
        enableTransition();
    }
    public List<AssetReference> AssetReferences = new List<AssetReference>();

    public static event Func<IEnumerator> LoadRequest;

    public AssetReference LoadingScreenPrefab;

    LoadingScreenView LoadingScreenView;
    public CanvasGroup transitionScreen;
    void enableTransition() {
        transitionScreen.alpha = 1;
        transitionScreen.blocksRaycasts = true;
        transitionScreen.interactable = true;
    }
    void disableTransition() {
        transitionScreen.alpha = 0;
        transitionScreen.blocksRaycasts = false;
        transitionScreen.interactable = false;
    }
    //public override IEnumerator SingletonStart() {
    //    Debug.Log(GetType().FullName + "::Initialize");
    //    var op = LoadingScreenPrefab.InstantiateAsync(transform);
    //    op.Completed += LoadingScreenLoadedCompleted;
    //    yield return op;
    //    op.Result.GetComponent<Canvas>().worldCamera = Camera.main;

    //    foreach (Func<IEnumerator> item in LoadRequest.GetInvocationList()) {
    //        yield return CoroutineUtils.RunThrowingIterator(item.Invoke(), ex => {
    //            if (ex != null) { Debug.LogError(ex.Message); }
    //        });
    //    }
    //    Completed += cbLoadingCompleted;
    //}

    private IEnumerator cbLoadingCompleted() {
        ViewManager.CloseView(LoadingScreenView);
        yield break;
    }
    private void LoadingScreenLoadedCompleted(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle<GameObject> obj) {
        disableTransition();
        GameObject go = obj.Result;
        LoadingScreenView = go.GetComponent<LoadingScreenView>();
        ViewManager.OpenView(LoadingScreenView);
        //obj.Result
    }

    void Update() {
        
    }

}
