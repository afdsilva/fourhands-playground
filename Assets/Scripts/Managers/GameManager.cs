﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class GameManager : Singleton<GameManager> {
    public override void SingletonAwake() {
        //LoadAssetManager.ModuleLoad += Init;
        registerCallbacks();
    }
    public Timestamp CurrentTime;
    public TextMeshProUGUI ClockText;
    public TextMeshProUGUI EventTestText;
    [SerializeField] private CharacterData Player;
    void registerCallbacks() {
        unregisterCallbacks();
        CurrentTime.OnTimeUpdate += cbOnTimeUpdate;
        LoadingManager.Completed += LoadingManager_Completed;
        CharacterManager.Completed += CharacterManager_Completed;
        EventData.OnBeginEvent += EventData_OnUpdateEvent;
    }

    private IEnumerator CharacterManager_Completed() {
        if (CharacterManager.Instance.GetCollectable("Player", out Player) == false) {
            Debug.LogError("Player Data not found");
        }
        yield break;
    }

    void unregisterCallbacks() {
        CurrentTime.OnTimeUpdate -= cbOnTimeUpdate;
        LoadingManager.Completed -= LoadingManager_Completed;
        CharacterManager.Completed -= CharacterManager_Completed;
        EventData.OnBeginEvent -= EventData_OnUpdateEvent;
    }
    private void EventData_OnUpdateEvent(EventData eventData) {
        EventTestText.text = eventData.EventText;
    }

    private IEnumerator LoadingManager_Completed() {
        //Debug.Log("Game Ready to events");
        CurrentTime.SetTimestamp(0, 0, 15);
        yield break;
    }

    private void cbOnTimeUpdate(Timestamp timestamp) {
        ClockText.text = timestamp.Day.ToString("00") + ":" + timestamp.Hour.ToString("00") + ":" + timestamp.Minute.ToString("00");
    }

    public void advanceTime() {
        CurrentTime.AddMinutes(15);
    }
}
