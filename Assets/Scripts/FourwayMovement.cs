﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Fourhands {
    [RequireComponent(typeof(Rigidbody2D)), RequireComponent(typeof(CircleCollider2D))]
    public class FourwayMovement : MonoBehaviour {
        public delegate float AxisEventDelegate(string axisName);
        public event AxisEventDelegate GetAxis;

        Rigidbody2D rb2d;
        Vector2 distanceToMove;
        Vector2 oldPos;
        float gravity; //talvez nao seja usado no Fourway
        public Vector2 Position {
            get { return transform.localPosition; }
            protected set { transform.localPosition = value; }
        }
        [Range(1, 100)]
        public float force = 10f;
        [Range(1,20)]
        public float maxVelocity = 10f;
        [Range(0,1)]
        public float friction = 0.15f;
        [Range(0, 100)]
        public float drag = 10;
        [Range(0,1)]
        public float walkThreshold = 0.01f;
        public ForceMode2D forceMode;
        public CharacterState state;
        public PlayerCharacterBehaviour playerBehaviour;
        void Start() {
            rb2d = GetComponent<Rigidbody2D>();
            if (GetAxis == null) {
                GetAxis = defaultGetAxis;
            }
        }

        private float defaultGetAxis(string axisName) {
            return Input.GetAxis(axisName);
        }
        void Update() {

            //Movement update here? ou no fixed?
            //OnUpdateCharacter()
            Vector2 axisValues = Vector2.zero;
            if (GetAxis != null) {
                axisValues.x = GetAxis("Horizontal");
                axisValues.y = GetAxis("Vertical");
            }

            Move(axisValues, forceMode);

            //Vector2 accel = new Vector2(axisValues.x * Time.deltaTime * Speed, axisValues.y * Time.deltaTime * Speed);
            //Position += accel;
            //playerMovement();
            //aiMovement();

            maxVelocityCheck();
        }

        void movementCheck() {
            //move o personagem
        }

        void maxVelocityCheck() {
            Vector2 velocity = rb2d.velocity;

            if (velocity.x > maxVelocity)
                velocity.x = maxVelocity;
            else if (velocity.x < -maxVelocity)
                velocity.x = -maxVelocity;
            else if (velocity.x > -walkThreshold && velocity.x < walkThreshold)
                velocity.x = 0;

            if (velocity.y > maxVelocity)
                velocity.y = maxVelocity;
            else if (velocity.y < -maxVelocity)
                velocity.y = -maxVelocity;
            else if (velocity.y > -walkThreshold && velocity.y < walkThreshold)
                velocity.y = 0;

            rb2d.velocity = velocity;
        }
        public void Move(Vector2 axisValues, ForceMode2D forceMode) {
            if (playerBehaviour != PlayerCharacterBehaviour.Control)
                return;
            state = CharacterState.Idle;
            //reduz a velocidade até chegar a 0;
            Vector2 velocity = rb2d.velocity;
            if (velocity.x > walkThreshold)
                velocity.x -= Time.deltaTime * friction * (drag * (1 - Mathf.Abs(axisValues.x)));
            else if (velocity.x < -walkThreshold)
                velocity.x += Time.deltaTime * friction * (drag * (1 - Mathf.Abs(axisValues.x)));
            else
                velocity.x = 0;
            if (velocity.y > walkThreshold)
                velocity.y -= Time.deltaTime * friction * (drag * (1 - Mathf.Abs(axisValues.y)));
            else if (velocity.y < -walkThreshold)
                velocity.y += Time.deltaTime * friction * (drag * (1 - Mathf.Abs(axisValues.y)));
            else
                velocity.y = 0;

            rb2d.AddForce(axisValues * force, forceMode);

            rb2d.velocity = velocity;
            //Vector2 diff = oldPos - Position;
            //float sqrLen = diff.sqrMagnitude;
            //if (sqrLen > walkThreshold * walkThreshold) {
            //    state = CharacterState.Walking;
            //}
            //oldPos = Position;
        }

        public void MoveTo(Vector2 dist) {

        }
        public enum CharacterState {
            Idle,
            Walking,
            Running,
            Rolling,
            Jumping
        }
        public enum PlayerCharacterBehaviour {
            Control,
            Static,
            Pathing
        }
    }
}