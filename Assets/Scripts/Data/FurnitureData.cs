﻿using System;
using UnityEngine;
using UnityEngine.AddressableAssets;
using FHSharedUtils;

[CreateAssetMenu(fileName = "New Furniture", menuName = "Data/Furniture")]
public class FurnitureData : FHSData {
    //public AssetReferenceSprite SpriteAsset;
    public Sprite NormalSprite;
    public Sprite HighlightSprite;
    public Sprite InteractedSprite;

    public override void DoAction(IActionable target, string action) {
        throw new NotImplementedException("Furniture can't have actions yet.");
    }

    public override string GetName() {
        return Name;
    }
}
[Serializable]
public class AssetReferenceFurnitureData : AssetReferenceT<FurnitureData> {
    public AssetReferenceFurnitureData(string guid) : base(guid) {

    }
}