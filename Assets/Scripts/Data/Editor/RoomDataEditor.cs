﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
[CustomEditor(typeof(RoomController))]

public class RoomDataEditor : Editor {
    RoomData roomData;
    private void OnEnable() {
        roomData = (RoomData)target;
        //roomData.LoadAsset();
    }
    public override void OnInspectorGUI() {
        base.OnInspectorGUI();
    }
}
