﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using FHSharedUtils;

[CreateAssetMenu(fileName = "New Room", menuName = "Data/Room")]
public class RoomData : FHSData {

    public Sprite Background;
    public List<FurnitureData> Furnitures = new List<FurnitureData>();

    [field: NonSerialized] public event Action<RoomData> OnEnterRoom;
    [field: NonSerialized] public event Action OnExitRoom;

    public override void DoAction(IActionable target, string action) {
        throw new NotImplementedException("Rooms don't have actions yet.");
    }

    public void EnterRoom() {
        OnEnterRoom?.Invoke(this);
    }
    public void ExitRoom() {
        OnExitRoom?.Invoke();
    }

    public override string GetName() {
        return Name;
    }
}
[Serializable]
public class AssetReferenceRoomData : AssetReferenceT<RoomData> {
    public AssetReferenceRoomData(string guid) : base(guid) {
        this.guid = guid;
    }
    public string guid;
}