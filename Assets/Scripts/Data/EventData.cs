﻿using UnityEngine;
using System;
using UnityEngine.AddressableAssets;
using FHSharedUtils;

[CreateAssetMenu(fileName = "New Event", menuName = "Data/Event")]
public class EventData : ScriptableObject {
    public string EventName;
    public string EventText;
    public Timestamp Time;

    public FHSAction actionOnTrigger;

    public delegate void EventDelegate(EventData eventData);
    [field: NonSerialized] public static event EventDelegate OnBeginEvent;

    bool isRunning;
    public void Begin() {
        OnBeginEvent?.Invoke(this);
        actionOnTrigger.Trigger();
    }

    public bool CanTrigger(Timestamp time) {
        Timestamp delay = new Timestamp(time.TotalMinutes + 5);
        bool check = Time.InRange(time, delay);
        //Debug.Log(EventName + " Time: " + Time.ToString() + "time: " + time.ToString() + " delay: " + delay.ToString() + " check " + check);
        return check;
    }
}
public class AssetReferenceEventData : AssetReferenceT<EventData> {
    public AssetReferenceEventData(string guid) : base(guid) {

    }
}
[Serializable]
public class FHSAction {

    public FHSData sourceData;
    public FHSData targetData;
    public string action;
    public void Trigger() {
        if (sourceData == null) return;
        sourceData.DoAction(targetData, action);
    }
}