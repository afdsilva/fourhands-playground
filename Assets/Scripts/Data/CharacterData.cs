﻿using System;
using UnityEngine;
using FHSharedUtils;

[CreateAssetMenu(fileName = "New Character", menuName = "Data/Character")]
public class CharacterData : FHSData {
    public delegate void ActionDelegate(IActionable target, string action);
    public event ActionDelegate OnAction;
    public override void DoAction(IActionable target, string action) {

        OnAction?.Invoke(target, action);
    }

    public override string GetName() {
        return Name;
    }
}
