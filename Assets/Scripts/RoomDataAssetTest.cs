﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;

[RequireComponent(typeof(SpriteRenderer))]
public class RoomDataAssetTest : MonoBehaviour {

    SpriteRenderer sr;
    public AssetReferenceRoomData RoomDataReference;
    [SerializeField] private RoomData roomData;
    public AssetReferenceFurnitureData FurnitureDataReference;
    [SerializeField] private FurnitureData furnitureData;
    void Start() {
        sr = GetComponent<SpriteRenderer>();
        RoomDataReference.LoadAssetAsync().Completed += cbRoomDataCompleted;
        FurnitureDataReference.LoadAssetAsync().Completed += cbFurnitureDataCompleted;
    }

    private void cbFurnitureDataCompleted(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle<FurnitureData> obj) {
        furnitureData = obj.Result;
        Debug.Log("Furniture: " + furnitureData.name + " IID: " + furnitureData.GetInstanceID());
    }

    private void cbRoomDataCompleted(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle<RoomData> obj) {
        roomData = obj.Result;
        sr.sprite = roomData.Background;
        Debug.Log("RoomData: " + roomData.name + " Furnitures: " + roomData.Furnitures.Count);
        for (int i = 0; i < roomData.Furnitures.Count; i++) {
            Debug.Log("Roomdata " + roomData.name + "Furniture: " + roomData.Furnitures[i].name + " IID: " + roomData.Furnitures[i].GetInstanceID());
        }
    }

    void Update() {
    }
}
