﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadingScreenView : ViewController {
    CanvasGroup canvasGroup;
    public override bool IsEnabled { get; protected set; }
    private void Start() {
        canvasGroup = GetComponent<CanvasGroup>();
    }
    public override void CloseView() {
        HideOverlay();
    }

    public override void OpenView() {
        ShowOverlay();
    }
    public void ShowOverlay() {
        IsEnabled = true;
        if (canvasGroup == null)
            canvasGroup = GetComponent<CanvasGroup>();
        canvasGroup.alpha = 1f;
        canvasGroup.interactable = true;
        canvasGroup.blocksRaycasts = true;
    }
    public void HideOverlay() {
        IsEnabled = false;
        if (canvasGroup == null)
            canvasGroup = GetComponent<CanvasGroup>();
        canvasGroup.alpha = 0f;
        canvasGroup.interactable = false;
        canvasGroup.blocksRaycasts = false;
    }
    public override void Unload() {
        throw new System.NotImplementedException();
    }
}