﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using TMPro;

public class EventManager : Singleton<EventManager> {
    public override void SingletonAwake() {
        //LoadAssetManager.ModuleLoad += Init;
    }
    //public TextMeshProUGUI loadedEventsText;
    List<EventData> loadedEvents = new List<EventData>();
    EventData currentEvent;
    
    public override void Awake() {
        base.Awake();
        LoadingManager.LoadRequest -= cbLoadRequest;
        LoadingManager.LoadRequest += cbLoadRequest;
        GameManager.Instance.CurrentTime.OnTimeUpdate -= cbOnTimeUpdate;
        GameManager.Instance.CurrentTime.OnTimeUpdate += cbOnTimeUpdate;

    }

    void cbOnTimeUpdate(Timestamp timestamp) {
        foreach (var item in loadedEvents) {
            if (item.CanTrigger(timestamp) == true) {
                currentEvent = item;
                currentEvent.Begin();
                //Debug.Log(currentEvent.EventText);
                return;
            }
        }
    }
    public IEnumerator cbLoadRequest() {
        //Debug.Log(GetType().FullName + "::cbLoadRequest");
        var op = Addressables.LoadAssetsAsync<EventData>("EventData", eventDataLoaded);
        op.Completed += eventsLoadedCompleted;
        //op.PercentComplete
        yield return op;

        //var op2 = RoomPrefab.LoadAssetAsync<GameObject>();
    }

    void eventDataLoaded(EventData data) {

        loadedEvents.Add(data);
        //Debug.Log("Loading " + data.name);
    }
    void eventsLoadedCompleted(AsyncOperationHandle<IList<EventData>> obj) {

        Debug.Log(obj.Result.Count + " events loaded.");
    }

}
