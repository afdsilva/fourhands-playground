﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridInfoManager : MonoBehaviour {
    public GameObject tileInfoPrefab;
    Dictionary<Vector3Int, tileInfoView> gridInfoDatabase = new Dictionary<Vector3Int, tileInfoView>();
    public Vector3 offset;
    private void Awake() {
        //OniNM.OniPathfinding.OnPathGenerated -= UpdatePath;
        //OniNM.OniPathfinding.OnPathGenerated += UpdatePath;
    }
    public void GenerateInfo() {
        if (gridInfoDatabase == null)
            gridInfoDatabase = new Dictionary<Vector3Int, tileInfoView>();
        gridInfoDatabase.Clear();

        GridManagerController.Instance.SelectionTilemap.CompressBounds();
        foreach (var pos in GridManagerController.Instance.SelectionTilemap.cellBounds.allPositionsWithin) {
            if (GridManagerController.Instance.SelectionTilemap.HasTile(pos) == true) {
                Vector3 worldPos = GridManagerController.Instance.SelectionTilemap.CellToWorld(pos);

                GameObject go = Instantiate(tileInfoPrefab, worldPos + offset, Quaternion.identity, this.transform);
                go.name = pos.ToString();
                tileInfoView tiv = go.GetComponent<tileInfoView>();
                tiv.Clear();
                gridInfoDatabase[pos] = tiv;
            }
        }

    }
    void Start() {
        GenerateInfo();
    }

    public void UpdatePath(OniNM.OniNode[] nodes) {
        foreach (var item in gridInfoDatabase) {
            item.Value.Clear();
        }
        for (int i = 0; i < nodes.Length; i++) {
            Vector3Int pos = nodes[i].TilePos;
            if (gridInfoDatabase.ContainsKey(pos)) {
                gridInfoDatabase[pos].UpdateNode(nodes[i]);
            }
        }

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
