﻿using UnityEngine;
using System.Collections.Generic;
using OniNM;

public abstract class SlotLegacyManager : MonoBehaviour {

}
public abstract class SlotLegacyManager<T> : SlotLegacyManager where T : SlotLegacy {

    public List<T> Slots = new List<T>();

    public bool GetSlot(int index, out T slot) {
        if (index < 0 || index >= Slots.Count) {
            slot = null;
            return false;
        }
        slot = Slots[index];
        return true;
    }
    public bool GetEmptySlot(out T slot) {
        slot = null;
        for (int i = 0; i < Slots.Count; i++) {
            if (Slots[i].IsEmpty) {
                slot = Slots[i];
                return true;
            }
        }
        return false;
    }
    public int CountEmptySlots() {
        int count = 0;
        for (int i = 0; i < Slots.Count; i++) {
            if (Slots[i].IsEmpty) {
                count++;
            }
        }
        return count;
    }
    public void AddSlot(T slot) {
        Slots.Add(slot);
        //slot.SlotManager = this;
    }
}
