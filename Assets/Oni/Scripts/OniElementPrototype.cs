﻿using UnityEngine;
using System;
using System.Collections.Generic;

namespace OniNM {
    [CreateAssetMenu(fileName = "NewElement", menuName = "Oni/Element")]
    public class OniElementPrototype : ScriptableObject {
        public string Id;
        public string Name;
        public string Description;
        public int Tier;
        public Sprite Icon;
        public OniSkillPrototype[] Skills;

        public Sprite getSprite() {
            return Icon;
        }
    }
}