﻿using UnityEngine;
using System.Collections;

namespace OniNM {
    [CreateAssetMenu(fileName = "NewSkillEffect", menuName = "Oni/Skill/New Effect")]
    public class OniSkillEffectPrototype : ScriptableObject {

        public string Id;
        public string Value;
        public string SkillEffectActionName;
        public OniSkill.Target SkillTarget;
        public OniSkillEffect.EffectType Type;
        public OniStance.StanceCondition StanceCondition;
        public OniStance.StancePhase StancePhase;
        public float Duration;
        public float Tick = 1f;
        public Sprite EffectIcon;
    }
}