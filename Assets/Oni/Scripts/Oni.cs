﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OniNM {
    public class Oni : ISlotable {
        protected Oni() {
        }

        protected OniElement element;
        protected Slot owner;
        protected Combatant source;

        public event Action<OniStance.OniStanceType> OnStanceUpdate;

        public OniSkillPrototype[] GetSkills() {
            OniSkillPrototype[] skillPrototypes = element.GetSkillPrototype(OniStance.OniStanceType.Any, true);
            //Debug.Log("GetSkills: " + skillPrototypes.Length);
            return skillPrototypes;
        }
        public void Update(float deltaTime) {
            //rockSkillSlot?.Update(deltaTime);
            //paperSkillSlot?.Update(deltaTime);
            //scissorSkillSlot?.Update(deltaTime);
        }

        #region ISlotable implementation
        public bool ValidSlot {
            get {
                if (owner == null) return false;
                Oni content;
                if (owner.GetContent(out content) == false) return false;
                return (content == this);
            }
        }
        public void EquipOn(Slot slot) {
            owner = slot;
        }
        public Slot GetSlot() {
            return owner;
        }
        #endregion

        public override string ToString() {
            return "[" + element.Name + " - " + source.Name + "]";
        }
        public static Oni Create(Combatant source, OniElementPrototype element, int tier) {
            Oni oni = new Oni();
            oni.element = OniElement.Instantiate(element);
            oni.source = source;
            return oni;
        }

    }
    public class OniException : Exception {
        public OniException() { }
        public OniException(string message) : base(message) { }
        public OniException(Oni oni) { }
    }
}