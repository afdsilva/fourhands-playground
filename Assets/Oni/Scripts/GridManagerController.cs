﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
public class GridManagerController : Singleton<GridManagerController> {
    public override void SingletonAwake() {
    }

    public Tilemap SelectionTilemap;
    public Tilemap CharacterTilemap;
    public Tilemap ZoneTilemap;
    public Tilemap CollisionTilemap;
    public Vector3 tileOffset = new Vector3(0.5f, 0.5f, 0);

    public LayerMask ObstacleLayerMask;
    public Tile allowedSelectionTile;
    public Tile forbiddenSelectionTile;
    public Tile normalSelectionTile;
    public Tile characterTile;

    public override void Awake() {
        base.Awake();
        OniNM.OniCharacter.OnUpdateTile -= cbOniCharacter_OnUpdateTile;
        OniNM.OniCharacter.OnUpdateTile += cbOniCharacter_OnUpdateTile;
    }
    public OniNM.OniCharacter[] cbOnProviderCharsInRange(Vector3Int tilePos, int sight) {
        //Debug.Log("cbOnProviderTarget tilePos: " + tilePos + " sight: " + sight);
        List<OniNM.OniCharacter> targetsInRange = new List<OniNM.OniCharacter>();
        for (int i = -sight; i <= sight; i++) {
            for (int j = -sight; j <= sight; j++) {
                //-2,-2 -2,-1 -2, 0 -2, 1 -2, 2
                //-1,-2 -1,-1 -1, 0 -1, 1 -1, 2
                // 0,-2  0,-1  0, 0  0, 1  0, 2
                // 1,-2  1,-1  1, 0  1, 1  1, 2
                // 2,-2  2,-1  2, 0  2, 1  2, 2
                if (Mathf.Abs(i) + Mathf.Abs(j) > sight + 1) continue;
                int x = tilePos.x + i;
                int y = tilePos.y + j;
                Vector3Int searchPos = new Vector3Int(x, y, tilePos.z);
                if (searchPos == tilePos) continue;
                //Debug.Log(searchPos);
                TileCharacter tileCharacter = CharacterTilemap.GetTile<TileCharacter>(searchPos);
                if (tileCharacter == null) continue;
                //Debug.Log("Source: "+ tilePos + " inRange at " + searchPos);
                OniNM.OniCharacter targetFound;
                if (tileCharacter.GetOniCharacter(searchPos, out targetFound) == true) {
                    targetsInRange.Add(targetFound);
                }
                
            }
        }
        return targetsInRange.ToArray();
    }
    public bool cbOnProviderCheckSight(OniNM.OniCharacter source, OniNM.OniCharacter target, float distance) {
        //distance = Vector3.Distance(source.Position, target.Position);
        Vector2 dir = (target.Position - source.Position).normalized;
        RaycastHit2D hit = Physics2D.Raycast(source.Position, dir, distance, LayerMask.GetMask("Obstacle"));
        
        return (hit.collider == null); //se ocorrer alguma colisao retorna falso, pois esta fora da linha de visao
    }

    private void cbOniCharacter_OnUpdateTile(Vector3Int oldPos, Vector3Int newPos) {
        CharacterTilemap.SetTile(oldPos, null);
        CharacterTilemap.SetTile(newPos, characterTile);
    }
    public void SelectPath(OniNM.OniNode[] path, int movementPoints) {
        ClearSelection();
        List<OniNM.OniNode> reversedPath = new List<OniNM.OniNode>(path);
        reversedPath.Reverse();
        for (int i = 0; i < path.Length; i++) {
            if (i <= movementPoints) {
                SelectionTilemap.SetTile(reversedPath[i].TilePos, allowedSelectionTile);
            } else {
                SelectionTilemap.SetTile(reversedPath[i].TilePos, forbiddenSelectionTile);
            }
        }
    }
    public void Select(Vector3 worldPos, int range, bool ignoreDiagonals = true) {
        Vector3Int tilePos = SelectionTilemap.WorldToCell(worldPos);

        ClearSelection();
        for (int i =  -range; i <= range; i++) {
            for (int j = -range; j <= range; j++) {
                Vector3Int checkPos = new Vector3Int(tilePos.x + i, tilePos.y + j, tilePos.z);
                if (ignoreDiagonals == true && Mathf.Abs(i) + Mathf.Abs(j) > range) continue;

                if (SelectionTilemap.HasTile(checkPos) == true) {
                    if (CollisionTilemap.HasTile(checkPos) == false) {
                        if (CollisionTilemap.CheckValidTileCollider(tilePos, checkPos, ObstacleLayerMask)) {
                            SelectionTilemap.SetTile(checkPos, allowedSelectionTile);
                        } else {
                            SelectionTilemap.SetTile(checkPos, forbiddenSelectionTile);
                        }
                    }
                }
            }
        }
    }
    public void ClearSelection() {
        SelectionTilemap.ClearSelection(normalSelectionTile);
    }
    public static bool CheckValid(Vector3Int tilePos) {
        //Debug.Log("CheckValid: " + tilePos + " " + Instance.SelectionTilemap.HasTile(tilePos));
        if (Instance.SelectionTilemap.HasTile(tilePos) == false) return false; //apenas tiles selecionaveis sao validos
        if (Instance.CollisionTilemap.HasTile(tilePos) == true) return false; //verifica se nao ha obstaculos no tile
        if (Instance.CharacterTilemap.HasTile(tilePos) == true) return false; //tiles que possuam outro personagem sao invalidos
        if (Instance.ZoneTilemap.GetTile<ZoneTile>(tilePos)?.Walkable == false) return false;
        return true;
    }
    public static float CheckCost(Vector3Int tilePos,bool isDiagonal) {
        float baseValue = 1 ;
        if (isDiagonal) baseValue = 1.414f;
        if (Instance.ZoneTilemap.HasTile(tilePos) == true) {
            ZoneTile zone = Instance.ZoneTilemap.GetTile<ZoneTile>(tilePos);
            if (zone != null) {
                baseValue += zone.Cost;
            }
        }
        return baseValue;
    }
    public enum TilemapType {
        Selecion,
        Character,
        Collision,
    }
    public static TilemapType defaultTilemapType = TilemapType.Character;
    public static Vector3Int WorldToCell(Vector3 pos) {
        switch(defaultTilemapType) {
            case TilemapType.Collision:
                return Instance.CollisionTilemap.WorldToCell(pos);
            case TilemapType.Selecion:
                return Instance.SelectionTilemap.WorldToCell(pos);
            default:
                return Instance.CharacterTilemap.WorldToCell(pos);
        }
    }
    public static Vector3 CellToWorldOffset(Vector3Int tilePos) {
        switch (defaultTilemapType) {
            case TilemapType.Collision:
                return Instance.CollisionTilemap.CellToWorldCenter(tilePos);
            case TilemapType.Selecion:
                return Instance.SelectionTilemap.CellToWorldCenter(tilePos);
            default:
                return Instance.CharacterTilemap.CellToWorldCenter(tilePos);
        }
    }
}
public static class TilemapUtils {
    public static void ClearSelection(this Tilemap tilemap, Tile tile) {
        foreach (var item in tilemap.cellBounds.allPositionsWithin) {
            if (tilemap.HasTile(item) == false) continue;
            tilemap.SetTile(item, tile);
        }
    }
}
