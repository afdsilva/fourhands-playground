﻿using UnityEngine;
using System;
using System.Collections.Generic;

namespace OniNM {
    public class OniSkill : ISlotable {

        protected OniSkill()  {
            skillEffects = new List<OniSkillEffectPrototype>();
        }
        protected OniSkill(OniSkillPrototype prototype) {
            Name = prototype.Name;
            Id = prototype.Id;
            Range = prototype.Range;
            Tier = prototype.Tier;
            Cost = prototype.Cost;
            Permanent = prototype.Permanent;
            Cooldown = prototype.Cooldown;
            Type = prototype.Type;
            Upgrades = prototype.Upgrades.Clone();
            skillEffects = new List<OniSkillEffectPrototype>(prototype.SkillEffects);
        }
        public string Id { get; protected set; }
        public string Name { get; protected set; }
        public int Range { get; protected set; }
        public int Tier { get; protected set; }
        public int Cost { get; protected set; }
        public bool Permanent { get; protected set; }
        public float Cooldown { get; protected set; }
        public Combatant Source { get; protected set; }
        public OniStance.OniStanceType Type { get; protected set; }
        public OniSkilltree Upgrades { get; protected set; }
        List<OniSkillEffectPrototype> skillEffects;

        public event Action<OniSkill> OnSkillUse;
        public event Action<OniSkill> OnSkillUpdate;
        public event Action<OniSkill> OnSkillAvailable;
        float cdTimer;
        public bool InCooldown { get { return (cdTimer > 0); } }
        public bool Use(int apAvailable, out Exception e) {
            e = null;
            if (cdTimer > 0) {
                e = new SkillException(this, "Skill in CD");
                return false;
            }
            if (apAvailable < Cost) {
                e = new SkillException(this, "No AP");
                return false;
            }
            cdTimer = Cooldown;
            OnSkillUse?.Invoke(this);
            return true;
        }
        public void Update(float deltaTime) {
            if (cdTimer > 0) {
                cdTimer -= deltaTime;
                if (cdTimer <= 0) {
                    OnSkillAvailable?.Invoke(this);
                }
            }
            OnSkillUpdate?.Invoke(this);
        }

        #region ISlotable Implementation
        Slot _ownerSlot;
        public bool ValidSlot { get { return (_ownerSlot == null); } }
        public void EquipOn(Slot slot) {
            _ownerSlot = slot;
        }

        public Slot GetSlot() {
            return _ownerSlot;
        }
        #endregion

        public OniSkillEffectPrototype[] GetEffects() {
            return skillEffects.ToArray();
        }
        public OniSkillEffectPrototype[] GetEffects(bool useFlag, params object[] filter) {
            List<OniSkillEffectPrototype> effects = new List<OniSkillEffectPrototype>(skillEffects);

            for (int i = 0; i < filter.Length; i++) {
                if (filter[i].GetType() == typeof(OniStance.StanceCondition)) {
                    OniStance.StanceCondition condition = (OniStance.StanceCondition)filter[i];
                    if (useFlag) {
                        effects = effects.FindAll(s => s.StanceCondition.HasFlag(condition));
                    } else {
                        effects = effects.FindAll(s => s.StanceCondition == condition);
                    }
                    //Debug.Log("condition: " + condition.ToString() + " effects: " + effects.Count);
                } else if (filter[i].GetType() == typeof(OniStance.StancePhase)) {
                    OniStance.StancePhase phase = (OniStance.StancePhase)filter[i];
                    if (useFlag) {
                        effects = effects.FindAll(s => s.StancePhase.HasFlag(phase));
                    } else {
                        effects = effects.FindAll(s => s.StancePhase == phase);
                    }
                    //Debug.Log("phase: " + phase.ToString() + " effects: " + effects.Count);
                } else if (filter[i].GetType() == typeof(Target)) {
                    Target skillTarget = (Target)filter[i];
                    if (useFlag) {
                        effects = effects.FindAll(s => s.SkillTarget.HasFlag(skillTarget));
                    } else {
                        effects = effects.FindAll(s => s.SkillTarget == skillTarget);
                    }
                    //Debug.Log("Target: " + skillTarget.ToString() + " effects: " + effects.Count);
                }
            }
            //Debug.Log("Effects.Count: " + effects.Count);
            return effects.ToArray();
        }
        public OniSkillEffect[] GetEffectsInstance(OniStance.StanceCondition stance, OniSkill.Target skillTarget, OniCharacter target) {
            OniSkillEffectPrototype[] prototypes = GetEffects(true, stance, skillTarget);
            List<OniSkillEffect> instances = new List<OniSkillEffect>();
            for (int i = 0; i < prototypes.Length; i++) {
                OniSkillEffect oniSkillEffect;
                if (OniSkillEffect.Instantiate(prototypes[i], this, target, out oniSkillEffect) == true) {
                    instances.Add(oniSkillEffect);
                }
            }

            return instances.ToArray();
        }
        public static OniSkill Instantiate(OniSkillPrototype prototype, Combatant source) {
            OniSkill skill = new OniSkill(prototype);
            skill.Source = source;
            return skill;
        }
        public static OniSkill Generate(OniElement element, Combatant source, OniStance.OniStanceType stance, int tier, int index = -1) {
            OniSkillPrototype[] protoSkills = element.GetSkillPrototype(stance);
            OniSkillPrototype choosenSkill;
            if (index > -1) {
                choosenSkill = protoSkills[index];
            } else {
                choosenSkill = protoSkills[UnityEngine.Random.Range(0, protoSkills.Length)];
            }
            return OniSkill.Instantiate(choosenSkill, source);
        }

        [Flags]
        public enum Target {
            None = 0,
            Self = 1,
            Other = 2,
            All = Self | Other,
            Friendly = 4,
            Enemies = 8,
            NoChange = 1024
        }
        public class SkillException : Exception {
            public SkillException() { Skill = null; }
            public SkillException(OniSkill skill, string message) : base(message) {
                Skill = skill;
            }
            public OniSkill Skill { get; protected set; }
        }
    }
    
}