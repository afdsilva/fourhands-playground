﻿using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using System;
using System.Collections;
using System.Collections.Generic;

public abstract class AddressableOperation : IEnumerator {
    public AddressableOperation(string location) {
        Location = location;
        Error = string.Empty;
    }
    public delegate void ProgressDelegate(string label, float value);
    public string Location { get; protected set; }
    public string Error { get; protected set; }
    public object Current { get { return null; } }
    abstract public bool Update();
    public bool MoveNext() {
        if (IsDone) {
            return false;
        }
        return true;
    }
    public abstract bool IsDone { get; }
    public abstract float PercentComplete { get; }
    public virtual void Reset() { }
}
public class RequestAddressableAsset<T> : AddressableOperation {
    public RequestAddressableAsset(string location) : base(location) {
    }
    protected AsyncOperationHandle<T> operationHandler;
    public event Action<AsyncOperationHandle<T>> Completed;
    public virtual event ProgressDelegate OnUpdateProgress;
    public override bool IsDone {
        get { return operationHandler.IsDone; }
    }
    public override float PercentComplete { get { return (operationHandler.IsValid() ? -1 : operationHandler.PercentComplete); } }

    bool init;
    public override bool Update() {
        if (init == false) {
            init = true;
            operationHandler = Addressables.LoadAssetAsync<T>(Location);
            operationHandler.Completed += OperationHandle_Completed;
        }

        OnUpdateProgress?.Invoke(Location, PercentComplete);
        return !IsDone;
    }
    private void OperationHandle_Completed(AsyncOperationHandle<T> obj) {
        Completed?.Invoke(obj);
    }
}
public class RequestAddressableCollection<T> : AddressableOperation {
    public RequestAddressableCollection(string location, Action<T> callback) : base(location) {
        completeCallback = callback;
    }
    Action<T> completeCallback;
    AsyncOperationHandle<IList<T>> operationHandle;
    public event ProgressDelegate OnUpdateProgress;
    public event System.Action<AsyncOperationHandle<IList<T>>> Completed;
    public override bool IsDone { get { return (operationHandle.IsValid() ? operationHandle.IsDone : false); } }
    public override float PercentComplete { get { return operationHandle.PercentComplete; } }
    bool init;
    public override bool Update() {
        if(init == false) {
            init = true;
            operationHandle = Addressables.LoadAssetsAsync<T>(Location, completeCallback);
            operationHandle.Completed += OperationHandle_Completed;
        }
        OnUpdateProgress?.Invoke(Location, PercentComplete);
        return !IsDone;
    }
    private void OperationHandle_Completed(AsyncOperationHandle<IList<T>> obj) {
        Completed?.Invoke(obj);
    }
}
/**
public class LoadAddressableAsset<T> : AddressableOperation {
    public LoadAddressableAsset(string address) {
        operationHandle = Addressables.LoadAssetAsync<T>(address);
        operationHandle.Completed += OperationHandle_Completed;
    }

    public new event Action<AsyncOperationHandle<T>> Completed;
    AsyncOperationHandle<T> operationHandle;
    public override float PercentComplete { get { return operationHandle.PercentComplete; } }
    public override bool IsDone { get { return operationHandle.IsDone; } }
    public override bool Update() {
        return IsDone;
    }

    public bool GetAsset(out T asset) {
        if (IsDone) {
            asset = operationHandle.Result;
            return true;
        }
        asset = default;
        return false;
    }
    private void OperationHandle_Completed(AsyncOperationHandle<T> obj) {
        Completed(obj);
    }
}
/** /
public class LoadAdressableAssets<T> : AddressableOperation {
    public LoadAdressableAssets(string address, Action<T> callback) : base(address) {
        operationHandle = Addressables.LoadAssetsAsync<T>(address, callback);
        operationHandle.Completed += OperationHandle_Completed;
    }
    public new event System.Action<AsyncOperationHandle<IList<T>>> Completed;
    AsyncOperationHandle<IList<T>> operationHandle;
    public override bool IsDone { get { return operationHandle.IsDone; } }
    public override float PercentComplete { get { return operationHandle.PercentComplete; } }
    public bool GetAssets(out IList<T> assets) {
        if (IsDone) {
            assets = operationHandle.Result;
            return true;
        }
        assets = default;
        return false;
    }
    private void OperationHandle_Completed(AsyncOperationHandle<IList<T>> obj) {
        Completed(obj);
    }

    public override bool Update() {
        throw new NotImplementedException();
    }
}
/**/
