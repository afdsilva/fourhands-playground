﻿using UnityEngine;
using System;
using System.Collections;

namespace OniNM {
    public class OniStance {
        private OniStance() { }
        public OniStance(Combatant combatant, OniStanceType type) {
            Combatant = combatant;
            Type = type;
        }
        public OniStanceType Type;
        public Combatant Combatant;
        //public OniComb

        public class OniStanceException : Exception {
            public OniStanceException(string message) : base(message) { }
        }
        [Flags]
        public enum OniStanceType {
            None = 0,
            Rock = 1,
            Paper = 2,
            Scissor = 4,
            Any = Rock | Paper | Scissor
        }
        [Flags]
        public enum StanceCondition {
            None = 0,
            Win = 1, //Quando ganha sendo o ator ativo da skill
            Lose = 2, //Quando perde sendo o ator passivo da skill
            Resist = 4, //Quando ganha sendo o ator passivo da skill
            Interrupted = 8, //Quando perde sendo o ator ativo da skill
            Draw = 16, //Sem vencedor
            Any = Win | Lose | Resist | Interrupted | Draw,
            Favorable = Win | Resist,
            Unfavorable = Lose | Interrupted,
            NoChange = 1024
        }
        public enum StancePhase {
            NotEngaged,
            Engaged,
            Begin,
            PreResolution,
            Resolution,
            End,
            Finalize
        }
    }
    public static class OniStanceExtensions {
        public static bool isBefore(this OniStance.StancePhase phase, OniStance.StancePhase otherPhase) {
            if (phase == otherPhase) return false;
            //Debug.Log(phase.ToString() + " isBefore " + otherPhase.ToString() );
            switch (phase) {
                case OniStance.StancePhase.NotEngaged:
                    return true; //sempre que o phase for NotEngaged entao qualquer valor de otherPhase vai ser verdadeiro
                case OniStance.StancePhase.Engaged:
                    if (otherPhase == OniStance.StancePhase.NotEngaged) return false;
                    break;
                case OniStance.StancePhase.Begin:
                    if (otherPhase == OniStance.StancePhase.NotEngaged ||
                        otherPhase == OniStance.StancePhase.Engaged) return false;
                    break;
                case OniStance.StancePhase.PreResolution:
                    if (otherPhase == OniStance.StancePhase.NotEngaged ||
                        otherPhase == OniStance.StancePhase.Begin ||
                        otherPhase == OniStance.StancePhase.Engaged) return false;
                    break;
                case OniStance.StancePhase.Resolution:
                    if (otherPhase == OniStance.StancePhase.NotEngaged ||
                        otherPhase == OniStance.StancePhase.Engaged ||
                        otherPhase == OniStance.StancePhase.PreResolution) return false;
                    break;
                case OniStance.StancePhase.End:
                    if (otherPhase == OniStance.StancePhase.NotEngaged ||
                        otherPhase == OniStance.StancePhase.Engaged ||
                        otherPhase == OniStance.StancePhase.PreResolution ||
                        otherPhase == OniStance.StancePhase.Resolution) return false;
                    break;
            }

            return true;

        }
        public static OniStance.StancePhase Next(this OniStance.StancePhase phase) {

            switch (phase) {
                case OniStance.StancePhase.Engaged:
                    return OniStance.StancePhase.Begin;
                case OniStance.StancePhase.Begin:
                    return OniStance.StancePhase.PreResolution;
                case OniStance.StancePhase.PreResolution:
                    return OniStance.StancePhase.Resolution;
                case OniStance.StancePhase.Resolution:
                    return OniStance.StancePhase.End;
                case OniStance.StancePhase.End:
                    return OniStance.StancePhase.Finalize;
                default:
                    return OniStance.StancePhase.NotEngaged;
            }
        }
        public static OniStance.StanceCondition Attack(this OniStance.OniStanceType myType, OniStance.OniStanceType otherType) {
            switch (myType) {
                case OniStance.OniStanceType.Rock:
                    if (otherType == OniStance.OniStanceType.Scissor) return OniStance.StanceCondition.Win;
                    if (otherType == OniStance.OniStanceType.Paper) return OniStance.StanceCondition.Interrupted;
                    break;
                case OniStance.OniStanceType.Paper:
                    if (otherType == OniStance.OniStanceType.Rock) return OniStance.StanceCondition.Win;
                    if (otherType == OniStance.OniStanceType.Scissor) return OniStance.StanceCondition.Interrupted;
                    break;
                case OniStance.OniStanceType.Scissor:
                    if (otherType == OniStance.OniStanceType.Paper) return OniStance.StanceCondition.Win;
                    if (otherType == OniStance.OniStanceType.Rock) return OniStance.StanceCondition.Interrupted;
                    break;

            }
            return OniStance.StanceCondition.Draw;
        }
        public static OniStance.StanceCondition Defend(this OniStance.OniStanceType myType, OniStance.OniStanceType otherType) {
            switch (myType) {
                case OniStance.OniStanceType.Rock:
                    if (otherType == OniStance.OniStanceType.Scissor) return OniStance.StanceCondition.Resist;
                    if (otherType == OniStance.OniStanceType.Paper) return OniStance.StanceCondition.Lose;
                    break;
                case OniStance.OniStanceType.Paper:
                    if (otherType == OniStance.OniStanceType.Rock) return OniStance.StanceCondition.Resist;
                    if (otherType == OniStance.OniStanceType.Scissor) return OniStance.StanceCondition.Lose;
                    break;
                case OniStance.OniStanceType.Scissor:
                    if (otherType == OniStance.OniStanceType.Paper) return OniStance.StanceCondition.Resist;
                    if (otherType == OniStance.OniStanceType.Rock) return OniStance.StanceCondition.Lose;
                    break;

            }
            return default;
        }
        public static OniStance.StanceCondition Negate(this OniStance.StanceCondition myCondition) {
            switch (myCondition) {
                case OniStance.StanceCondition.Win:
                    return OniStance.StanceCondition.Lose;
                case OniStance.StanceCondition.Lose:
                    return OniStance.StanceCondition.Win;
                case OniStance.StanceCondition.Resist:
                    return OniStance.StanceCondition.Interrupted;
                case OniStance.StanceCondition.Interrupted:
                    return OniStance.StanceCondition.Resist;
                case OniStance.StanceCondition.Favorable:
                    return OniStance.StanceCondition.Unfavorable;
                case OniStance.StanceCondition.Unfavorable:
                    return OniStance.StanceCondition.Favorable;
                case OniStance.StanceCondition.Any:
                    return OniStance.StanceCondition.Any;
                default:
                    return OniStance.StanceCondition.None;
            }
        }
        public static OniStance.OniStanceType Negate(this OniStance.OniStanceType myType) {
            switch (myType) {
                case OniStance.OniStanceType.Rock:
                    return OniStance.OniStanceType.Paper;
                case OniStance.OniStanceType.Paper:
                    return OniStance.OniStanceType.Scissor;
                case OniStance.OniStanceType.Scissor:
                    return OniStance.OniStanceType.Rock;
                default:
                    return OniStance.OniStanceType.None;
            }
        }
    }
}