﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using OniNM;

public class OniStanceController : MonoBehaviour {
    public Button stanceShortcut1;
    public Button stanceShortcut2;
    public Button stanceShortcut3;
    public Button stanceShortcut4;
    List<OniKeyBind> binds = new List<OniKeyBind>();

    OniCharacter player;

    public TMPro.TextMeshProUGUI actionPointsValue;
    void Awake() {

        OniKeyBind rock = new OniKeyBind() {
            label = "Rock",
            keyCode = KeyCode.A,
            actionCallback = ChooseRock,
            button = stanceShortcut1
        };
        //rock.Setup();
        binds.Add(rock);
        OniKeyBind paper = new OniKeyBind() {
            label = "Paper",
            keyCode = KeyCode.S,
            actionCallback = ChoosePaper,
            button = stanceShortcut4
        };
        //paper.Setup();
        binds.Add(paper);
        OniKeyBind scissor = new OniKeyBind() {
            label = "Scissor",
            keyCode = KeyCode.D,
            actionCallback = ChooseScissor,
            button = stanceShortcut3
        };
        //scissor.Setup();
        binds.Add(scissor);
        OniKeyBind skill = new OniKeyBind() {
            label = "Skill",
            keyCode = KeyCode.Space,
            //actionCallback = chooseSkill, //the implementation of updateValidCallback/updateNotValidCallback is used on actionCallback
            button = stanceShortcut2,
            checkValid = checkSkillUsage,
            updateValidCallback = updateValidSkill,
            updateNotValidCallback = updateNotValidSkill,
        };
        //attack.Setup();
        binds.Add(skill);

        OniCharacterCollection.OnInitCharactersComplete -= cbOnInitCharactersComplete;
        OniCharacterCollection.OnInitCharactersComplete += cbOnInitCharactersComplete;
    }
    private void cbOnInitCharactersComplete(OniCharacterCollection manager) {
        if (OniCharacterCollection.GetPlayerCharacter(out player) == true) {
            player.OnActionPointsChanged -= cbOnActionPointsChanged;
            player.OnActionPointsChanged += cbOnActionPointsChanged;
            cbOnActionPointsChanged(player.ActionPoints);
            player.OnDefaultStanceChange -= UpdateStance;
            player.OnDefaultStanceChange += UpdateStance;
            UpdateStance(player.DefaultStance);
        }
    }
    private void cbOnActionPointsChanged(int obj) {
        if (actionPointsValue == null) return;
        actionPointsValue.text = obj.ToString();
    }

    public void UpdateStance(OniStance.OniStanceType stance) {
        stanceShortcut1.interactable = true;
        stanceShortcut4.interactable = true;
        stanceShortcut3.interactable = true;
        switch (stance) {
            case OniStance.OniStanceType.Rock:
                stanceShortcut1.interactable = false;
                break;
            case OniStance.OniStanceType.Paper:
                stanceShortcut4.interactable = false;
                break;
            case OniStance.OniStanceType.Scissor:
                stanceShortcut3.interactable = false;
                break;
        }
    }
    private void Update() {
        foreach (OniKeyBind bind in binds) {
            bind.Update();
        }
    }

    void chooseStance(OniStance.OniStanceType stance) {
        OniCharacter player;
        if (OniCharacterCollection.GetPlayerCharacter(out player) == true) {
            player.DefaultStance = stance;
            
        }
    }
    void ChooseRock() {
        chooseStance(OniStance.OniStanceType.Rock);
    }
    void checkRockUsage() {
        OniCharacter player = null;
        if (OniCharacterCollection.GetPlayerCharacter(out player) == false)
            throw new System.Exception("No Player found");
        //if (player.CurrentStance == OniStanceType.Rock)
    }
    void ChoosePaper() {
        chooseStance(OniStance.OniStanceType.Paper);
    }

    void ChooseScissor() {
        chooseStance(OniStance.OniStanceType.Scissor);
    }
    bool checkSkillUsage() {
        OniCharacter player = null;
        try {
            //pega o player atual
            if (OniCharacterCollection.GetPlayerCharacter(out player) == false)
                throw new System.Exception("No Player found");

            //verifica se a skill pode ser usada
            return player.CheckSkillInRange();
        } catch (ExceptionTarget) {
            //alvo nao eh valido, forca o jogador procurar o proximo alvo
            if (player.NextTarget() == true)
                return player.CheckSkillInRange(); //
        } catch (OniSkill.SkillException e) { // 
            Debug.LogError(e.Message + " -- skill nunca pode ser uma excecao aceitavel aqui, provavelmente foi mal configurado o jogador.");
            //player.SelectSkill(Skill.Punch);
            //return player.CheckSkillInRange();
        } catch (System.Exception e) {
            Debug.LogError(e.Message);
        }
        return false;
    }
    bool updateValidSkill(OniKeyBind bind) {
        OniCharacter player = null;
        if (OniCharacterCollection.GetPlayerCharacter(out player) == false)
            throw new System.Exception("No Player found");
        bind.label = "Use Skill";
        bind.actionCallback = player.UseSkill;
        return player.CheckSkillInRange();
    }
    bool updateNotValidSkill(OniKeyBind bind) {
        OniCharacter player = null;
        if (OniCharacterCollection.GetPlayerCharacter(out player) == false)
            throw new System.Exception("No Player found");
        bind.label = "Change Target";
        bind.actionCallback = () => { player.NextTarget(); };
        return true;
    }
    public class OniKeyBind {
        public string label;
        public KeyCode keyCode;
        public UnityEngine.Events.UnityAction actionCallback;
        public System.Func<bool> checkValid;
        public Button button;
        TMPro.TextMeshProUGUI textMesh;
        public System.Func<OniKeyBind, bool> updateValidCallback;
        public System.Func<OniKeyBind, bool> updateNotValidCallback;
        bool isValid;
        bool isInitialized;
        public bool alwaysEnabled;
        public void Setup() {
            isInitialized = true;
            isValid = false;
            textMesh = button.GetComponentInChildren<TMPro.TextMeshProUGUI>();
            button.onClick.RemoveAllListeners();
            button.onClick.AddListener(listenerHook);

            //updateValid(false);

        }
        void listenerHook() {
            actionCallback?.Invoke();
        }
        void updateButton(bool interactable) {
            if (textMesh != null)
                textMesh.text = label + "(" + keyCode.ToString() + ")";
            button.interactable = interactable;
        }
        void updateValid(bool valid) {
            if (isValid != valid) return;
            isValid = valid;

            bool interactable = true;
            if (isValid == true) {
                interactable = updateValidCallback == null ? true : updateValidCallback(this);
            } else {
                interactable = updateNotValidCallback == null ? true : updateNotValidCallback(this);
            }
            updateButton(interactable);
        }
        public void Update() {
            if (isInitialized == false) Setup();
            bool newIsValid = checkValid == null ? true : checkValid();
            updateValid(newIsValid);

            if (Input.GetKeyDown(keyCode) == true && button.interactable) {
                actionCallback?.Invoke();
            }

        }
    }
}
