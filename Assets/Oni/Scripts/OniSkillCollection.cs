﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using OniNM;
using UnityEngine.AddressableAssets;
using System;

public class OniSkillCollection : Singleton<OniSkillCollection> {
    public override void SingletonAwake() {
        LoadAssetManager.ModuleLoad += Init;
        LoadAssetManager.Initialize += LoadAssetManager_Initialize;
        //LoadAssetManager.Completed += LoadAssetManager_Completed;
    }
    private IEnumerator LoadAssetManager_Initialize() {
        yield return LoadAssetManager.RequestLoadCollection<OniSkillPrototype>("OniSkill", skillLoad_Completed);
    }
    void skillLoad_Completed(OniSkillPrototype skillPrototype) {
        //Debug.Log("LOADED OniSKillPrototype: " + skillPrototype.Name);
        if (skillPrototype.Id == "basicRock") {
            _basicRock = skillPrototype;
        } else if (skillPrototype.Id == "basicPaper") {
            _basicPaper = skillPrototype;
        } else if (skillPrototype.Id == "basicScissor") {
            _basicScissor = skillPrototype;
        } else {
            skillPrototypes.Add(skillPrototype);
        }
    }

    [SerializeField] List<OniSkillPrototype> skillPrototypes = new List<OniSkillPrototype>();

    [SerializeField] OniSkillPrototype _basicRock;
    [SerializeField] OniSkillPrototype _basicPaper;
    [SerializeField] OniSkillPrototype _basicScissor;

    public static OniSkillPrototype BasicRock {
        get {
            return Instance._basicRock;
        }

    }
    public static OniSkillPrototype BasicPaper { get { return Instance._basicPaper; } }
    public static OniSkillPrototype BasicScissor { get { return Instance._basicScissor; } }

}
