﻿using System;
using System.Collections;
using UnityEngine;
using OniNM;

public class RPSIconController : MonoBehaviour {

    public GameObject Gfx;
    public SpriteRenderer iconSR;
    public SpriteRenderer checkmarkSR;
    public Vector2 offset;
    public Sprite greenCheckmark;
    public Sprite redCheckmark;
    public Sprite defaultCheckmark;

    public Sprite rock;
    public Sprite paper;
    public Sprite scissor;

    public float timerCD = 3f;
    public float afterDoneTimerCD = 3f;

    OniStance.StanceCondition currentCondition;
    OniSkillEffect[] currentEffects;
    Action<OniSkillEffect[], bool> currentCallback;
    bool currentToggleSkillCD;
    Action OnTimerFinish;
    bool isDone;
    float afterDoneTimer;
    float timer;
    bool isEnabled;
    public void ShowStance(OniStance.OniStanceType stance, OniStance.StanceCondition condition, OniSkillEffect[] effects, bool toggleSkillCD, Action<OniSkillEffect[], bool> resolutionCallback) {
        isEnabled = true;
        isDone = false;
        timer = timerCD;
        afterDoneTimer = afterDoneTimerCD;
        currentCondition = condition;
        currentEffects = effects;
        currentCallback = resolutionCallback;
        currentToggleSkillCD = toggleSkillCD;
        switch (stance) {
            case OniStance.OniStanceType.Rock:
                iconSR.sprite = rock;
                break;
            case OniStance.OniStanceType.Paper:
                iconSR.sprite = paper;
                break;
            case OniStance.OniStanceType.Scissor:
                iconSR.sprite = scissor;
                break;
        }
        checkDefault();
        if (condition == OniStance.StanceCondition.Win) {
            OnTimerFinish = checkGreen;
        } else if (condition == OniStance.StanceCondition.Lose) {
            OnTimerFinish = checkRed;
        } else {
            OnTimerFinish = checkRed;
        }
    }
    void checkGreen() {
        Gfx.SetActive(true);
        checkmarkSR.sprite = greenCheckmark;
    }
    void checkRed() {
        Gfx.SetActive(true);
        checkmarkSR.sprite = redCheckmark;
    }
    void checkDefault() {
        Gfx.SetActive(true);
        checkmarkSR.sprite = defaultCheckmark;
    }
    public void Hide() {
        Gfx.SetActive(false);
    }
    void Start() {
        Hide();
    }

    void Update() {
        float time = Time.deltaTime;
        handleTimer(time);
        handleAfterDoneTimer(time);
    }
    void handleTimer(float time) {
        if (isEnabled == false || isDone == true) return;
        timer -= time;
        if (timer <= 0) {
            isDone = true;
            OnTimerFinish?.Invoke();
        }

    }
    void handleAfterDoneTimer(float time) {
        if (isEnabled == false || isDone == false) return;
        afterDoneTimer -= time;
        if (afterDoneTimer <= 0) {
            isEnabled = false;
            currentCallback?.Invoke(currentEffects, currentToggleSkillCD);
            Hide();
        }

    }
}
