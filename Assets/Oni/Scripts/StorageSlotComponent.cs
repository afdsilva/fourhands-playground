﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using OniNM;
using TMPro;
public class StorageSlotComponent : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler, IDropHandler {

    public StorageSlotLegacy StorageSlot { get; protected set; }
    public TMP_Text Label;
    public Image Icon;
    Vector3 initialIconPos;
    bool isDragging;
    public void SetSlot(StorageSlotLegacy storageSlot = null) {
        unregisterCallbacks();
        StorageSlot = storageSlot;
        //StorageSlot_OnSlotChange(StorageSlot.Oni);
        if (StorageSlot != null) registerCallbacks();
        UpdateStorage();
    }
    void registerCallbacks() {
        unregisterCallbacks();
        if (StorageSlot != null) StorageSlot.OnSlotChange += UpdateStorage;
    }
    void unregisterCallbacks() {
        if (StorageSlot != null) StorageSlot.OnSlotChange -= UpdateStorage;
    }
    void Awake() {
    }
    private void UpdateStorage() {
        try {
            ISlotable slotContent;
            if (StorageSlot.GetContent(out slotContent) == false) throw new System.Exception("content not available");

            Oni oni = (Oni)slotContent;
            //Label.text = oni.Element.Name;
            //Icon.sprite = oni.Element.Icon;

        } catch {
            Label.text = "";
            Icon.sprite = null;
        }
    }

    public void OnBeginDrag(PointerEventData eventData) {
        if (StorageSlot.IsEmpty) return;
        isDragging = true;
        initialIconPos = Icon.transform.position;
        SlotLegacy.SlotTransaction = StorageSlot;

        Icon.transform.SetParent(Icon.canvas.transform);
    }
    public void OnDrag(PointerEventData eventData) {
        if (isDragging == false) return;
        Vector3 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        pos.z = 0f;
        Icon.transform.position = pos;
    }
    public void OnEndDrag(PointerEventData eventData) {
        if (isDragging == false) return;
        isDragging = false;
        Icon.transform.position = initialIconPos;
        Icon.transform.SetParent(this.transform);
        SlotLegacy.SlotTransaction = null;
    }
    public void OnDrop(PointerEventData eventData) {
        throw new System.NotImplementedException();
        //if (Slot.SlotTransaction == null || Slot.SlotTransaction.IsEmpty) return;
        //if (SkillSlot.SwitchOnis(Slot.SlotTransaction, StorageSlot) == false) {
        //    Debug.LogError("Can't switch oni from slots");
        //}
        /**
        Oni oni;

        if (Slot.SlotTransaction.Unequip(out oni) == true) {
            if (StorageSlot.Equip(oni) == false) {
                //Debug.LogWarning("Storage already ocupped.");
                Oni switchedOni;
                if (StorageSlot.Unequip(out switchedOni) == true) {
                    Slot.SlotTransaction.Equip(switchedOni);
                    StorageSlot.Equip(oni);
                }
                
            }
        } else {
            Debug.LogWarning("No valid Oni to move.");
        }
        **/
    }
}
