﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace OniNM {
    public interface ICombatant {
        string Name { get; }
        SkillManager GetSkillManager();
        OniStance[] GetStances();
        OniStance.OniStanceType DefaultStance { get; }
        OniStance GetStance(ICombatant target);
        void ChangeStance(ICombatant target, OniStance.OniStanceType newStance);
        OniStance.StanceCondition GetStanceCondition(ICombatant target, bool isAttacker);
        bool InCombat(ICombatant target);
        OniSkill GetSkill(ICombatant enemy);
        void UseSkill(ICombatant target);
        void Damage(int value);
        void Heal(int value);
        void StepCombatVisuals(OniStance.StanceCondition condition, OniStance.StancePhase phase, Combatant enemy, bool isAttacker, Action<OniStance.StanceCondition, OniStance.StancePhase, Combatant, Combatant, bool> callback);
    }
    public interface IEffectable {
        string Name { get; }
        OniSkillEffect[] GetEffects();
        void ApplyEffect(OniSkillEffect skillEffect);
        void ApplyEffects(OniSkillEffect[] skillEffects, OniStance.StancePhase nextPhase, Combatant enemy, bool isAttacker, Action<OniStance.StancePhase, Combatant, Combatant, bool> callback);
    }
    public interface IMoveable {
        void SetPosition(Vector3 position);
        Vector3 GetPosition();
        void MoveTo(Vector3 target, float maxSpeed, Action<Exception> cbOnCompleteMove = null, Action<Exception> cbOnCancelMove = null);

    }
    public interface IPathable {
        Vector3Int GetTile();
        void PathTo(Vector3Int targetTile, System.Action<Exception> cbOnPathEnd = null, System.Action<Exception> cbOnPathCancel = null);
    }
    public abstract class Combatant {
        protected Combatant() {
        }
        public virtual void Update(float deltaTime) {
            handleEffects(deltaTime);
        }
        public string Name { get; protected set; }
        public SkillManager SkillManager { get; protected set; }

        #region COMBAT
        public event Action<OniStance.OniStanceType> OnDefaultStanceChange;
        public event Action<Combatant, OniStance.OniStanceType> OnStanceChange;
        public event Action<Exception> OnSkillFail;
        List<OniStance> oniStances = new List<OniStance>();
        OniStance.OniStanceType _defaultStance = OniStance.OniStanceType.Rock;
        public OniStance.OniStanceType DefaultStance {
            get {
                return _defaultStance;
            }
            set {
                _defaultStance = value;

                //_currentStance.GetSkill(out currentSkill, currentOni);
                OnDefaultStanceChange?.Invoke(_defaultStance);
            }
        }

        public HitPoint TotalHP;

        public void ChangeStance(Combatant target, OniStance.OniStanceType newStance) {
            OniStance stance;
            if (getStance(target, out stance) == false) {
                //faz alguma coisa na criacao da stance?
            }
            stance.Type = newStance;
            OnStanceChange?.Invoke(target, newStance);
        }
        protected bool getStance(Combatant target, out OniStance stance) {
            stance = oniStances.Find(s => s.Combatant == target);
            if (stance == null) {
                stance = new OniStance(target, DefaultStance);
                oniStances.Add(stance);
                return false;
            }
            return true;
        }
        public OniStance.OniStanceType GetStanceType(Combatant target) {
            OniStance stance;
            if (getStance(target, out stance) == false) {
                //faz alguma coisa na criacao de stance?
            }
            return stance.Type;
        }
        public OniStance.StanceCondition GetStanceCondition(Combatant target, bool isAttacker) {
            OniStance.OniStanceType myType = GetStanceType(target);
            OniStance.OniStanceType enemyType = target.GetStanceType(this);
            if (isAttacker)
                return myType.Attack(enemyType);
            else
                return myType.Defend(enemyType);
        }
        public void Damage(int value) {
            TotalHP.Damage(value);
            //TotalHP.Change(value);
        }
        public void Heal(int value) {
            TotalHP.Heal(value);
        }

        public void useSkillTest(Combatant target) {
            CombatPhase combat = new CombatPhase(this, target);
            combat.Begin();
        }
        public bool InCombat(Combatant target) {
            throw new NotImplementedException();
            //CombatPhase attackerAlreadyInCombat = activeCombat.Find(c => c.Attacker == target);
            //return (attackerAlreadyInCombat != null);
        }
        public void UseSkill(Combatant target) {
            
            CombatPhase combat = new CombatPhase(this, target);
            //target.Engage(combat);
            combat.Begin();
        }
        public void UseSkill() {
            //try {
            //    Oni oni;
            //    if (OniSlot.GetContent(out oni) == false) throw new Exception("Oni not found.");
            //    if (characterIsLocked == true) {
            //        OnSkillFail?.Invoke(new Exception("can't use skill, character locked. "));
            //        return;
            //    }
            //    //if (oni.GetSkill(CurrentStance, out mySkill) == false) throw new Exception("Skill not found.");

            //    Exception e;
            //    if (oni.UseSkill(ActionPoints, CurrentStance, out e) == false) {
            //        throw e;
            //    }
            //    OniCharacter target = null;
            //    if (GetCurrentTarget(out target) == false) throw new ExceptionTarget();
            //    CombatPhase combat = new CombatPhase(this, target);
            //    combat.Begin();

            //} catch (ExceptionTarget e) {
            //    OnSkillFail?.Invoke(e);
            //} catch (OniStance.OniStanceException e) {
            //    OnSkillFail?.Invoke(e);
            //} catch (OniSkill.SkillException e) {
            //    OnSkillFail?.Invoke(e);
            //} catch (Exception e) {
            //    Debug.LogError(e.Message);
            //}
            //return;
        }
        public abstract OniSkill GetSkill(Combatant enemy);

        public bool CheckSkillInRange() {
            throw new OniSkill.SkillException();
            //Debug.Log("CheckSkillInRange");
            //OniCharacter target;
            //if (GetCurrentTarget(out target) == false) {
            //    throw new ExceptionTarget();
            //}
            //Debug.Log("CheckSkillInRange target" + target.Name);
            //OniSkill skill;
            //if (GetCurrentSkill(out skill) == false) {
            //    throw new OniSkill.SkillException();
            //}
            //if (Mathf.Abs(target.Tile.x - Tile.x) > skill.Range ||
            //    Mathf.Abs(target.Tile.y - Tile.y) > skill.Range)
            //    return false;
            //return true;
        }
        #endregion

        #region EFFECTS
        protected List<OniSkillEffect> currentEffects = new List<OniSkillEffect>();
        protected List<OniSkillEffect> effectsLog = new List<OniSkillEffect>();
        protected virtual void handleEffects(float deltaTime) {
            if (currentEffects == null) currentEffects = new List<OniSkillEffect>();
            for (int i = 0; i < currentEffects.Count; i++) {
                OniSkillEffect effect = currentEffects[i];
                if (effect.Update(deltaTime) == false) {

                    currentEffects.Remove(effect); //remove dos efeitos atuais
                    effectsLog.Add(effect); //adiciona no log
                    i--;
                    continue;
                }
            }
        }
        public void ApplyEffect(OniSkillEffect skillEffect) {
            //Debug.Log("ApplyEffect " + Name + " skillEffect " + skillEffect.Id);
            //skillEffect.Init(this); // init is called with
            currentEffects.Add(skillEffect);
            skillEffect.Reset();

        }
        public event Action<OniSkillEffect[], OniStance.StancePhase, Combatant, Combatant, bool, Action<OniStance.StancePhase, Combatant, Combatant, bool>> OnApplyEffects;
        public void ApplyEffects(OniSkillEffect[] skillEffects, OniStance.StancePhase nextPhase, Combatant enemy, bool isAttacker, Action<OniStance.StancePhase, Combatant, Combatant, bool> callback) {
            if (OnApplyEffects == null) {
                for (int i = 0; i < skillEffects.Length; i++) {
                    ApplyEffect(skillEffects[i]);
                }
                callback?.Invoke(nextPhase, this, enemy, isAttacker);
            } else {
                OnApplyEffects(skillEffects, nextPhase, this, enemy, isAttacker, callback);
            }
        }
        protected int effectCounter(OniSkillEffect.EffectType effectType) {
            List<OniSkillEffect> effects = currentEffects.FindAll(e => e.Type == effectType);
            return effects.Count;
        }
        #endregion

        public virtual event Action<OniStance.StanceCondition, OniStance.StancePhase, Combatant, Combatant, bool, Action<OniStance.StanceCondition, OniStance.StancePhase, Combatant, Combatant, bool>> OnShowCombat;
        public void StepCombatVisuals(OniStance.StanceCondition condition, OniStance.StancePhase phase, Combatant enemy, bool isAttacker, Action<OniStance.StanceCondition, OniStance.StancePhase, Combatant, Combatant, bool> callback) {
            if (OnShowCombat == null) {
                callback?.Invoke(condition, phase, enemy, this, isAttacker);
            } else {
                OnShowCombat(condition, phase, this, enemy, isAttacker, callback);
            }
        }
    }

    public class OniCharacter : Combatant {
        protected OniCharacter() : base() {
            //storageSlots = new List<StorageSlot>();//TODO: StorageManager
            //currentPhase = OniStance.StancePhase.Engaged;
        }
        public bool IsPlayer { get; protected set; }
        protected bool characterIsLocked;
        public OniManager OniManager { get; protected set; }
        //protected List<StorageSlot> storageSlots;  //TODO: StorageManager
        public override void Update(float deltaTime) {
            base.Update(deltaTime);
            handleActionPoint(deltaTime);
            handleMovement(deltaTime);
            handleSight(deltaTime);
            handleTargeting(deltaTime);
        }
        public static bool ProvidersRegistered() {
            bool providers = !(providerWorldToCell == null || providerCellToWorldOffset == null || providerValidPosition == null || providerCostPosition == null);
            return providers;
        }
        public static void RegisterProviders(Func<Vector3, Vector3Int> cbOnProviderWorldToCell, Func<Vector3Int, Vector3> cbOnProviderCellToWorldOffset, Func<Vector3Int, bool> cbOnProviderValidPosition, Func<Vector3Int, bool, float> cbOnProviderCostPosition) {
            providerWorldToCell = cbOnProviderWorldToCell;
            providerCellToWorldOffset = cbOnProviderCellToWorldOffset;
            providerValidPosition = cbOnProviderValidPosition;
            providerCostPosition = cbOnProviderCostPosition;
        }

        #region Combatant Implementation
        public override OniSkill GetSkill(Combatant enemy) {
            OniStance.OniStanceType stanceType = GetStanceType(enemy);
            OniSkill skill;
            if (SkillManager.GetSkill(stanceType, out skill) == false) {
                Debug.LogError(Name + " not found skill for stance " + stanceType.ToString() + " against enemy " + enemy.Name);
            }
            return skill;
        }
        #endregion

        #region ACTION_POINTS
        public event Action<int> OnActionPointsChanged;
        int _actionPoints;
        const int MAX_ACTION_POINTS = 10;
        public int ActionPoints {
            get { return _actionPoints; }
            protected set {
                if (_actionPoints == value) return;
                _actionPoints = Mathf.Clamp(value, 0, MAX_ACTION_POINTS);
                OnActionPointsChanged?.Invoke(_actionPoints);
            }
        }
        float apBaseCD = 10;
        public float ApCD {
            get { return apBaseCD; }
        }
        float apcdInternalTimer;
        public event Action<float, float> OnAPTimerUpdate;
        public float ApCDTimer {
            get {

                return apcdInternalTimer;
            }
            protected set {
                if (apcdInternalTimer == value) return;
                apcdInternalTimer = Mathf.Clamp(value, 0, ApCD);
                if (apcdInternalTimer <= 0) {
                    apcdInternalTimer = ApCD;
                    ActionPoints++;
                }
                OnAPTimerUpdate?.Invoke(apcdInternalTimer, ApCD);
            }
        }
        void handleActionPoint(float deltaTime) {
            if (ActionPoints >= MAX_ACTION_POINTS) return;
            ApCDTimer -= deltaTime;

        }

        #endregion

        #region MOVEMENT
        Vector3 _currentPosition;
        Vector3 _targetPosition;
        Vector3Int _currentTile;
        //MovementState movementState;
        bool isPathing;
        Queue<OniNode> path;
        List<OniNode> lastKnowFakePath;
        Vector3Int fakeTarget;

        Action<Exception> OnMoveEnd;
        Action<Exception> OnMoveCancel;
        Action<Exception> OnPathEnd;
        Action<Exception> OnPathCancel;
        static Func<Vector3, Vector3Int> providerWorldToCell;
        static Func<Vector3Int, Vector3> providerCellToWorldOffset;
        static Func<Vector3Int, bool> providerValidPosition;
        static Func<Vector3Int, bool, float> providerCostPosition;
        public event Action<Vector3> OnUpdatePosition;
        public static event Action<Vector3Int, Vector3Int> OnUpdateTile; //old, novo

        public Vector3Int Tile {
            get { return _currentTile; }
            protected set {
                Vector3Int old = _currentTile;
                _currentTile = value;
                if (_currentTile != old) OnUpdateTile?.Invoke(old, _currentTile);
            }
        }
        public Vector3 Position {
            get { return _currentPosition; }
            protected set {
                Vector3 old = _currentPosition;
                _currentPosition = value;
                if (_currentTile != old) OnUpdatePosition?.Invoke(_currentPosition);
            }
        }
        public Vector3 TilePosition {
            get {
                return providerCellToWorldOffset(_currentTile);
            }

        }
        public float Speed { get; protected set; }

        bool canMove() {
            bool moveAllowed = true;
            if (effectCounter(OniSkillEffect.EffectType.stun) > 0) moveAllowed = false;
            if (characterIsLocked) {
                Debug.Log("canMove() = characterIsLocked == true ");
                moveAllowed = false;
            }
            return moveAllowed;
        }
        bool isMoving;
        //movimento padrao, esse movimento eh irrestrito, portanto é chamado somente internamente
        void handleMovement(float deltaTime) {
            if (isMoving == false) return;

            if (canMove() == false) {
                Debug.Log("Can't move");
                return;
            }
            //centerCharacterInTile(deltaTime);

            //só realiza o movimento quando requisitado
            try {
                Position = Vector3.MoveTowards(Position, _targetPosition, Speed * deltaTime);
                Tile = providerWorldToCell(Position);
                //Debug.Log("Position: " + Position + " _targetPosition: " + _targetPosition);
                if (Vector3.Distance(Position, _targetPosition) < 0.001f) {
                    Position = _targetPosition;
                    isMoving = false;
                    OnMoveEnd?.Invoke(new Exception("End of Move"));
                }
            } catch (Exception e) {
                Debug.LogWarning(e.Message);
                isMoving = false;
            }
        }
        int movementCost {
            get {
                List<OniSkillEffect> skillEffects = currentEffects.FindAll(e => e.Type == OniSkillEffect.EffectType.apcost);
                return skillEffects.Count + 1;
            }
        }
        void handleNextNode(Exception e) {
            if (isPathing == false) return;
            try {
                if (path.Count <= 0) {
                    pathEnd(new Exception("End of Path"));
                    return;
                }

                if (canMove() == false) {
                    pathCanceled(new Exception("Can't move"));
                    return;
                }
                int movementCostBuffer = movementCost;
                if (ActionPoints < movementCost) {
                    pathCanceled(new Exception("End of Action Points"));
                    return;
                }
                ActionPoints -= movementCostBuffer;
                OniNode nextNode = path.Dequeue();

                if (providerCellToWorldOffset == null) throw new NullReferenceException("ProviderCellToWorldOffset not setted.");
                //Debug.Log("handleNextNode: " + nextNode.tilePos + " ActionPoints: " + ActionPoints);
                moveTo(providerCellToWorldOffset(nextNode.TilePos), handleNextNode, pathCanceled);
            } catch (Exception ex) {
                Debug.LogWarning(ex.Message);
            }
        }
        void pathEnd(Exception e) {
            //Debug.Log("path.Count: " + path.Count + " _movement.x: " + _movement.x);
            isPathing = false;
            OnPathEnd?.Invoke(e);
            isMoving = false;
            OnPathEnd = null;
            OnPathCancel = null;
        }
        void pathCanceled(Exception e) {
            isPathing = false;
            OnPathCancel?.Invoke(e);
            OnPathCancel = null;
            OnPathEnd = null;
        }
        public void MoveTo(Vector3 target, System.Action<Exception> cbOnCompleteMove = null, System.Action<Exception> cbOnCancelMove = null) {
            moveTo(target, cbOnCompleteMove, cbOnCancelMove);
        }
        void moveTo(Vector3 target, System.Action<Exception> cbOnCompleteMove = null, System.Action<Exception> cbOnCancelMove = null) {
            if (canMove() == false) return; //a funcao eh responsavel por verificar se pode se movimentar ou nao
            isMoving = true;
            OnMoveEnd = cbOnCompleteMove;
            OnMoveCancel = cbOnCancelMove;
            _targetPosition = target;
            //Debug.Log("moveTo: " + _targetPosition);
        }
        void centerCharacterInTile(float deltaTime) {
            //if (canMove() == false) return;

            try {
                if (providerCellToWorldOffset == null) throw new NullReferenceException("OnProviderCellToWorldOffset not setted.");

                //Tilemap characterTilemap = OnProviderCharacterTilemap?.Invoke();
                //Vector3 centerTilePosition = characterTilemap.CellToWorldCenter(Tile);
                Vector3 centerTilePosition = providerCellToWorldOffset(Tile);
                if (Vector3.Distance(Position, centerTilePosition) > 0.001f) {
                    Position = Vector3.MoveTowards(Position, centerTilePosition, Speed * deltaTime);
                }

            } catch (Exception e) {
                Debug.LogWarning(e.Message);
            }
        }
        //movimento instantaneo para a posicao indicada
        void teleport(Vector3 pos) {
            throw new NotImplementedException("teleport method not yet implemented.");
        }

        public void Path(Vector3 target, System.Action<Exception> cbOnPathEnd = null, System.Action<Exception> cbOnPathCancel = null) {
            Vector3Int targetTile = providerWorldToCell(target);
            Path(targetTile, cbOnPathEnd, cbOnPathCancel);
        }
        public void Path(Vector3Int targetTile, System.Action<Exception> cbOnPathEnd = null, System.Action<Exception> cbOnPathCancel = null) {
            OniPathfinding oniPathfinding = new OniPathfinding(providerValidPosition, providerCostPosition);
            if (oniPathfinding.Search(Tile, targetTile, out path) == true) {
                isPathing = true;
                OnPathEnd = cbOnPathEnd;
                OnPathCancel = cbOnPathCancel;
                path.Dequeue(); //sempre remove o primeiro elemento
                handleNextNode(null);
            } else {
                //Debug.Log("Path invalid.");
                cbOnPathCancel?.Invoke(new Exception("Path not Found"));
            }
        }
        public bool FakePathPosition(Vector3 target, out OniNode[] nodes) {
            Vector3Int targetCell = providerWorldToCell(target);
            if (lastKnowFakePath == null) lastKnowFakePath = new List<OniNode>();
            nodes = lastKnowFakePath.ToArray();
            if (fakeTarget == targetCell) return false;
            fakeTarget = targetCell;
            OniPathfinding oniPathfinding = new OniPathfinding(providerValidPosition, providerCostPosition);
            OniNode[] fakeNodes;
            if (oniPathfinding.Search(Tile, targetCell, out fakeNodes) == true) {
                nodes = fakeNodes;
                return true;
            }
            return false;
        }
        public OniCharacter CheckPosition(Vector3Int tile) {
            //Debug.Log("CheckPosition Tile: " + Tile + " tile: " + tile );
            if (Tile == tile) return this;
            return null;
        }
        #endregion

        #region SELECTION
        public static event Func<OniCharacter,bool> OnClearSelection;
        public event Action<OniCharacter> OnSelection;
        public bool Select() {
            //Debug.Log(GetType().FullName + "::Select::" + this.ToString());
            bool clearedSelf = false;
            if (OnClearSelection != null) {
                foreach (Func<OniCharacter, bool> item in OnClearSelection.GetInvocationList()) {
                    clearedSelf = item.Invoke(this) == true ? true : clearedSelf;
                }
            }
            if (clearedSelf == false) OnSelection?.Invoke(this);
            return !clearedSelf;
        }
        #endregion

        #region TARGET_SYSTEM

        public int Sight = 3;
        Func<Vector3Int, int, OniCharacter[]> providerCharactersInSight;
        Func<OniCharacter, OniCharacter, float, bool> providerCheckSight;
        public event Action<OniCharacter, bool> OnInSight;
        //public event Action<OniCharacter> OnTarget;

        void handleTargeting(float deltaTime) {
        }

        public void registerSightProviders(Func<Vector3Int, int, OniCharacter[]> sightedCharacters, Func<OniCharacter, OniCharacter, float, bool> checkSight) {
            providerCharactersInSight = sightedCharacters;
            providerCheckSight = checkSight;
        }
        //OniCharacter lastKnowEnemy;
        public void InSight(OniCharacter target, bool inSight) {
            OnInSight?.Invoke(target, inSight);
        }

        List<OniCharacter> lastKnowCharactersInSight = new List<OniCharacter>();
        OniCharacter currentTarget;
        int currentTargetIndex {
            get {
                if (lastKnowCharactersInSight == null || lastKnowCharactersInSight.Count <= 0 || currentTarget == null) return -1;
                for (int i = 0; i < lastKnowCharactersInSight.Count; i++) {
                    if (lastKnowCharactersInSight[i] == currentTarget) return i;
                }
                return -1;
            }
        }
        public void SelectTarget(OniCharacter target) {
            if (CheckSight(target) == true)
                currentTarget = target;
        }
        public bool NextTarget() {
            if (lastKnowCharactersInSight == null || lastKnowCharactersInSight.Count <= 0) {
                //Debug.LogWarning(GetType().FullName + "::NextTarget::No targets found.");
                currentTarget = null;
                return false;
            } else {
                int nextTargetIndex = (currentTargetIndex + 1) % lastKnowCharactersInSight.Count;
                Debug.Log(GetType().FullName + "::NextTarget::currentTargetIndex: " + currentTargetIndex + " nextTargetIndex: " + nextTargetIndex + " Count: " + lastKnowCharactersInSight.Count);
                currentTarget = lastKnowCharactersInSight[nextTargetIndex];
                return true;
            }
        }
        public bool NearestTarget(out OniCharacter target, bool selectNearest = false) {
            if (lastKnowCharactersInSight == null || lastKnowCharactersInSight.Count <= 0) {
                target = null;
                return false;
            } else {
                int nearestTargetIndex = 0;
                float nearestDistance = 999;
                for (int i = 0; i < lastKnowCharactersInSight.Count; i++) {
                    OniCharacter c = lastKnowCharactersInSight[i];
                    float dist = Vector3.Distance(Position, c.Position);
                    if (dist < nearestDistance) {
                        nearestDistance = dist;
                        nearestTargetIndex = i;
                    }
                }
                if (GetKnowTarget(nearestTargetIndex, out target) == false) return false;
                if (selectNearest)
                    currentTarget = target;
                return true;
            }
        }
        public bool GetKnowTarget(int index, out OniCharacter target) {
            if (index < 0 || index >= lastKnowCharactersInSight.Count) {
                target = null;
                return false;
            }
            target = lastKnowCharactersInSight[index];
            return true;
        }
        public bool GetCurrentTarget(out OniCharacter target) {
            if (currentTargetIndex == -1 || lastKnowCharactersInSight == null || lastKnowCharactersInSight.Count <= 0) {
                target = null;
                return false;
            }
            target = lastKnowCharactersInSight[currentTargetIndex];
            return true;
        }
        public bool CheckSight(OniCharacter target) {
            if (providerCheckSight == null) throw new NullReferenceException("No ProviderSight setted.");
            float distance = Vector3.Distance(Position, target.Position);
            return providerCheckSight(this, target, distance);
        }
        void foundSight(OniCharacter source) {
            if (lastKnowCharactersInSight.Contains(source) == false) lastKnowCharactersInSight.Add(source);
            source.InSight(this, true);
        }
        bool loseSight(OniCharacter source) {
            source.InSight(this, false);
            return lastKnowCharactersInSight.Remove(source);
        }
        public void handleSight(float deltaTime) {
            //sightedCharacters = null;
            try {
                if (providerCharactersInSight == null) throw new NullReferenceException("No providerCharactersInSight setted.");

                List<OniCharacter> knowTargets = new List<OniCharacter>(lastKnowCharactersInSight);
                List<OniCharacter> targetsInRange = new List<OniCharacter>(providerCharactersInSight(Tile, Sight));
                //Debug.Log(Name + " targets found " + targetsInRange.Count);
                //foreach (var item in targetsInRange) {
                //    if (CheckSight(item) == true) {
                //        item.InRange(this, true);
                //        Debug.DrawLine(Position, item.Position, Color.green);
                //    } else {
                //        item.InRange(this, false);
                //        Debug.DrawLine(Position, item.Position, Color.red);
                //    }
                //}

                foreach (OniCharacter t in knowTargets) {
                    //if (t == this) continue; (this should never happen
                    if (targetsInRange.Contains(t) == false) loseSight(t);
                }
                foreach (OniCharacter t in targetsInRange) {
                    if (t == this) continue;
                    if (knowTargets.Contains(t) == false) foundSight(t);
                }
            } catch (NullReferenceException e) {
                Debug.LogError(e.Message);
            } catch (Exception e) {
                Debug.Log(e.Message);
            }

        }

        public Vector3 Direction(OniCharacter target) {
            Vector3 dir = (target.TilePosition - TilePosition).normalized;
            return dir;
        }

        #endregion

        public static OniCharacter Create(string name, bool isPlayer, Vector3 initialPosition, OniElementPrototype defaultElement) {
            if (ProvidersRegistered() == false) {
                throw new NullReferenceException("Providers not registered.");
            }
            OniCharacter character = new OniCharacter();
            character.Name = name;
            character.IsPlayer = isPlayer;

            character._targetPosition = character.Position = initialPosition;
            character.Tile = providerWorldToCell(initialPosition);
            character.TotalHP = new HitPoint(100, null);
            character.Speed = 2f;
            character.ApCDTimer = character.ApCD;
            character.ActionPoints = 5;
            character.SkillManager = SkillManager.Create(character);
            character.OniManager = OniManager.Create(character);

            Oni oni = Oni.Create(character, OniElementCollection.PhysicalElement, 0);
            character.OniManager.Equip(oni);
            //character.oniSlot = OniSlot.Create(character, oni);
            //storageslot ta meio confuso
            //for (int i = 0; i < 5; i++) {
            //    StorageSlot storageSlot = StorageSlot.Create(character);
            //    character.AddSlot(storageSlot);
            //}
            return character;
        }
    }

    public class CombatPhase {
        public CombatPhase(Combatant attacker, Combatant defender) {
            Attacker = attacker;
            Defender = defender;
            __attackerPhase = OniStance.StancePhase.NotEngaged;
            __defenderPhase = OniStance.StancePhase.NotEngaged;
        }
        public Combatant Attacker { get; protected set; }
        public Combatant Defender { get; protected set; }
        private OniStance.StancePhase __attackerPhase, __defenderPhase;
        public OniStance.StancePhase CurrentPhase {
            get {

                if (__attackerPhase.isBefore(__defenderPhase)) return __attackerPhase; //a antes
                else if (__defenderPhase.isBefore(__attackerPhase)) return __defenderPhase; // b antes
                return __defenderPhase; //nao importa
            }
        }
        
        public static event Action<OniStance.StanceCondition, OniStance.StancePhase, Action> OnShowVisuals;
        public void Begin() {
            //Debug.Log("CombatPhase::Engage");
            Debug.Log("Combat Begin: Attacker HP: " + Attacker.TotalHP.Value + "/" + Attacker.TotalHP.MaxValue + "  Defender HP: " + Defender.TotalHP.Value + "/" + Defender.TotalHP.MaxValue);
            OniStance.OniStanceType attackerStance = Attacker.GetStanceType(Defender);
            OniStance.OniStanceType defenderStance = Defender.GetStanceType(Attacker);
            Debug.Log("Attacker Stance: " + attackerStance + " Defender Stance: " + defenderStance);
            //Attacker.logCombat(this);
            syncPhaseChange(OniStance.StancePhase.Engaged, Attacker, Defender, true);
            syncPhaseChange(OniStance.StancePhase.Engaged, Defender, Attacker, false);
        }
        protected void syncPhaseChange(OniStance.StancePhase phase, Combatant current, Combatant enemy, bool isAttacker) {
            OniStance.StanceCondition currentCondition = OniStance.StanceCondition.None;
            OniStance.StanceCondition enemyCondition = OniStance.StanceCondition.None;

            if (isAttacker) {
                __attackerPhase = phase;
            } else {
                __defenderPhase = phase;
            }

            //Debug.Log("syncPhaseChange: CurrentPhase " + CurrentPhase.ToString() + " phase: " + phase.ToString() + " current: " + current.Name);
            if (CurrentPhase == phase) {
                switch (phase) {
                    case OniStance.StancePhase.Begin:
                        currentCondition = OniStance.StanceCondition.Any;
                        enemyCondition = OniStance.StanceCondition.Any;
                        break;
                    case OniStance.StancePhase.PreResolution:
                        currentCondition = OniStance.StanceCondition.Any;
                        enemyCondition = OniStance.StanceCondition.Any;
                        break;
                    case OniStance.StancePhase.Resolution:
                        currentCondition = current.GetStanceCondition(enemy, isAttacker);
                        enemyCondition = enemy.GetStanceCondition(current, !isAttacker);
                        break;
                    case OniStance.StancePhase.End:
                        currentCondition = current.GetStanceCondition(enemy, isAttacker);
                        enemyCondition = enemy.GetStanceCondition(current, !isAttacker);
                        break;
                    case OniStance.StancePhase.Finalize:
                        end();
                        return;
                }

                if (OnShowVisuals == null) {
                    current.StepCombatVisuals(currentCondition, phase, enemy, isAttacker, applyEffectsCallback);
                    enemy.StepCombatVisuals(enemyCondition, phase, current, !isAttacker, applyEffectsCallback);

                    //Attacker.StepCombatVisuals(currentCondition, phase, applyEffectsCallback);
                    //Defender.StepCombatVisuals(currentCondition, phase, applyEffectsCallback);
                } else {
                    OnShowVisuals(currentCondition, phase, () => {
                        current.StepCombatVisuals(currentCondition, phase, enemy, isAttacker, applyEffectsCallback);
                        enemy.StepCombatVisuals(enemyCondition, phase, current, !isAttacker, applyEffectsCallback);
                        //Attacker.StepCombatVisuals(currentCondition, phase, applyEffectsCallback);
                        //Defender.StepCombatVisuals(currentCondition, phase, applyEffectsCallback);
                    });
                }
            }
        }
        protected void applyEffectsCallback(OniStance.StanceCondition condition, OniStance.StancePhase phase, Combatant current, Combatant enemy, bool isAttacker) {
            //aplica efeitos de troca de etapa
            OniSkillEffectPrototype[] prototypes = new OniSkillEffectPrototype[0];
            OniSkillEffect[] effectsInstances;
            List<OniSkillEffect> effects = new List<OniSkillEffect>();
            OniSkill mySkill = current.GetSkill(enemy);
            OniSkill enemySkill = enemy.GetSkill(current);

            prototypes = mySkill.GetEffects(true, phase, condition, OniSkill.Target.Self);
            if (OniSkillEffect.Instantiate(prototypes, mySkill, current, out effectsInstances) == true) {
                effects.AddRange(effectsInstances);
            }
            prototypes = enemySkill.GetEffects(true, phase, condition.Negate(), OniSkill.Target.Other);
            if (OniSkillEffect.Instantiate(prototypes, enemySkill, current, out effectsInstances) == true) {
                effects.AddRange(effectsInstances);
            }

            current.ApplyEffects(effects.ToArray(), phase.Next(), enemy, isAttacker, syncPhaseChange);
        }
        void end() {
            Debug.Log("end: Attacker HP: " + Attacker.TotalHP.Value + "/" + Attacker.TotalHP.MaxValue + "  Defender HP: " + Defender.TotalHP.Value + "/" + Defender.TotalHP.MaxValue);
        }
        public void Abort() {
            throw new NotImplementedException("CombatPhase:::Abort");
        }
    }

    public class HitPoint {
        public HitPoint(int value, OniElementPrototype element, int maxValue = -1) {
            this.MaxValue = maxValue == -1 ? value : value;
            this.Value = value;
            this.Element = element;
        }
        [field: NonSerialized] public event Action<int, int> OnHPChange;
        int _maxValue;
        public int MaxValue {
            get { return _maxValue; }
            protected set {
                int diff = value - _maxValue;
                _maxValue = value;
                if (diff > 0) {
                    //maxvalue aumentou, entao o Value também eh aumentado
                    Value += diff;

                } else if (diff < 0) {
                    //maxvalue diminuiu, verifica se o Value ultrapassa o novo valor do _maxValue, como o proprio Value se deixa
                    //ser maior que o MaxValue, apenas "reatribui" o proprio valor, do contrario nao faz nada, apenas atualiza o view
                    if (Value < _maxValue)
                        Value = Value;
                    else
                        OnHPChange?.Invoke(Value, _maxValue);
                }
            }
        }
        int _value;
        public int Value {
            get { return _value; }
            protected set {
                _value = Mathf.Clamp(value, 0, MaxValue);
                OnHPChange?.Invoke(_value, MaxValue);
            }
        }
        public OniElementPrototype Element;

        public void Damage(int value) {
            //sempre causa pelo menos 1 de dano?
            //se sim, o que acontece com a mitigacao? sempre gasta? ou preserva?
            //por padrao, a mitigacao pode reduzir a 0
            //value = 2 miti = 2, realDamage = 0, remaining = 0
            //value = 3 miti = 5, realDamage = 0, remaining = 2
            //value = 2 miti = 1, realDamage = 1, remaining = 0
            int realDamageDealt = Mathf.Clamp(value - MitigateValue, 0, value);
            int remainingMitigation = Mathf.Clamp(MitigateValue - value, 0, MitigateValue);
            MitigateValue = remainingMitigation;
            Value -= realDamageDealt;
        }
        public void Heal(int value) {
            Value += value;
        }
        public int MitigateValue { get; protected set; }
        public void Mitigation(int value) {
            MitigateValue += value;
        }
    }
    public class ExceptionTarget : Exception {
        public ExceptionTarget() { Target = null; }
        public ExceptionTarget(OniCharacter target) {
            Target = target;
        }
        public OniCharacter Target { get; protected set; }
    }
}