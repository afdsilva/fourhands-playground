﻿using UnityEngine;
using System;
using System.Collections.Generic;

namespace OniNM {
    [Serializable]
    public class OniSkillEffect {
        protected OniSkillEffect() {
        }
        protected OniSkillEffect(OniSkillEffectPrototype prototype, OniSkill skill, Combatant target) {
            Id = prototype.Id;
            Value = prototype.Value;
            SkillEffectActionName = prototype.SkillEffectActionName;

            if (createAction(SkillEffectActionName, out skillEffectAction) == false) {
                Debug.LogError(Id + " nao pode criar action: " + SkillEffectActionName);
            }
            SkillTarget = prototype.SkillTarget;
            Type = prototype.Type;
            StanceCondition = prototype.StanceCondition;
            StancePhase = prototype.StancePhase;
            Duration = prototype.Duration;
            durationTimer = Duration;
            Tick = prototype.Tick;
            tickTimer = 0;
            SkillSource = skill;
            Target = target;
        }

        public string Id { get; protected set; }
        public string Value { get; protected set; }
        public string SkillEffectActionName { get; protected set; }
        Action<Combatant, OniSkillEffect, Combatant> skillEffectAction;
        public OniSkill.Target SkillTarget { get; protected set; }
        public EffectType Type { get; protected set; }
        public OniStance.StanceCondition StanceCondition { get; protected set; }
        public OniStance.StancePhase StancePhase { get; protected set; }
        public float Duration { get; protected set; }
        public float Tick { get; protected set; }
        public Sprite EffectIcon { get; protected set; }
        public OniSkill SkillSource { get; protected set; }
        public Combatant Target { get; protected set; }
        public bool IsDone { get; protected set; }

        float durationTimer;
        float tickTimer;

        public bool Update(float deltaTime) {
            //Debug.Log(GetType().FullName + "::Update::" + Id);
            if (IsDone == true) return false;
            tickTimer -= deltaTime;
            if (tickTimer <= 0) {
                //deve executar pelo menos 1x e entrar em "cooldown", esse eh tipo um clock interno para efeitos de tempo
                tickTimer = Tick;

                skillEffectAction?.Invoke(SkillSource.Source, this, Target);
            }
            durationTimer -= deltaTime;
            if (durationTimer <= 0) {
                IsDone = true;
                return false;
            }

            return true;
        }
        public void Reset() {
            //Debug.Log("durationTimer: " + durationTimer + " Duration " + Duration);
            IsDone = false;
            durationTimer = Duration;
            tickTimer = 0;
        }
        public bool GetInt(out int value) {
            return int.TryParse(Value, out value);
        }
        public bool GetBool(out bool value) {
            return bool.TryParse(Value, out value);
        }
        public void SetValue(int value) {
            Value = value.ToString();
        }
        public void SetValue(bool value) {
            Value = value.ToString();
        }
        public void SetValue(float value) {
            Value = value.ToString();
        }

        static bool createAction(string actionName, out Action<Combatant, OniSkillEffect, Combatant> action) {
            action = null;
            if (string.IsNullOrEmpty(actionName)) return false;

            Type t = typeof(SkillEffectActions);
            System.Reflection.MethodInfo methodInfo = t.GetMethod(actionName);
            action = (Action<Combatant, OniSkillEffect, Combatant>)Delegate.CreateDelegate(typeof(Action<Combatant, OniSkillEffect, Combatant>), methodInfo);

            return (action != null);
        }

        public static bool Instantiate(OniSkillEffectPrototype prototype, OniSkill source, Combatant target, out OniSkillEffect instance) {
            instance = new OniSkillEffect(prototype, source, target);
            return (instance != null);
        }
        public static bool Instantiate(OniSkillEffectPrototype[] prototypes, OniSkill source, Combatant target, out OniSkillEffect[] instances) {
            List<OniSkillEffect> effects = new List<OniSkillEffect>();
            for (int i = 0; i < prototypes.Length; i++) {
                OniSkillEffect effect;
                if (Instantiate(prototypes[i], source, target, out effect) == true)
                    effects.Add(effect);
            }
            instances = effects.ToArray();
            return instances.Length > 0;
        }

        public enum EffectType {
            none,
            damage,
            heal,
            mitigate,
            apcost,
            stun,
            root,
            forceMovement,
            forceStance,
            forceNegateStance,
            nochange,
        }
    }

    public class ExceptionSkillEffect : Exception {
        public ExceptionSkillEffect() { SkillEffect = null; }
        public ExceptionSkillEffect(OniSkillEffect skillEffect, string message) : base(message) {
            SkillEffect = skillEffect;
        }
        public OniSkillEffect SkillEffect { get; protected set; }
    }
    public static class SkillEffectActions {
        public static void Damage(Combatant source, OniSkillEffect effect, Combatant target) {
            //Debug.Log("SkillEffectActions.Damage");
            int value;
            if (int.TryParse(effect.Value, out value) == true) target.Damage(value);
        }
        public static void Mitigate(Combatant source, OniSkillEffect effect, Combatant target) {
            //Debug.Log("SkillEffectActions.Mitigate");
            int value;
            if (int.TryParse(effect.Value, out value) == true) target.TotalHP.Mitigation(value);
        }
        public static void Heal(Combatant source, OniSkillEffect effect, Combatant target) {
            //Debug.Log("SkillEffectActions.Heal");
            int value;
            if (int.TryParse(effect.Value, out value) == true) target.Heal(value);
        }
        public static void ForceStance(Combatant source, OniSkillEffect effect, Combatant target) {
            switch (effect.Type) {
                case OniSkillEffect.EffectType.forceStance:
                    OniStance.OniStanceType stanceType;
                    if (Enum.TryParse(effect.Value, out stanceType) == true) {
                        target.ChangeStance(source, stanceType);
                    }
                    //target.ChangeStance(source, )
                    break;
                case OniSkillEffect.EffectType.forceNegateStance:
                    target.ChangeStance(source, source.GetStanceType(target).Negate());
                    break;
                default:
                    Debug.LogError("Type" + effect.Type.ToString());
                    break;
            }
        }
    }

}