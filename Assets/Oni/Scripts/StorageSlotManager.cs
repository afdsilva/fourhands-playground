﻿using UnityEngine;
using System.Collections.Generic;
using OniNM;
using System.Collections;

public class StorageSlotManager : Singleton<StorageSlotManager> {
    public override void SingletonAwake() {
        //LoadAssetManager.ModuleLoad += Init;
    }

    public GameObject storageSlotPrefab;
    public Transform UIContainer;
    public Transform fill;
    //public int storageSlotsNum = 5;

    OniCharacter currentOniCharacter;
    public void SetCharacter(OniCharacter oniCharacter) {
        clearSlots();
        if (oniCharacter == null) return;
        currentOniCharacter = oniCharacter;
        //StorageSlotLegacy[] slots = currentOniCharacter.GetSlots();
        //for (int i = 0; i < slots.Length; i++) {
        //    StorageSlotComponent storageSlotComponent = CreateSlotComponent(slots[i], i);
        //    storageComponents.Add(storageSlotComponent);
        //}
    }
    List<StorageSlotComponent> storageComponents = new List<StorageSlotComponent>();
    void clearSlots() {
        for (int i = storageComponents.Count - 1; i >= 0; i--) {
            StorageSlotComponent storage = storageComponents[i];
            storage.SetSlot();
            Destroy(storage.gameObject);
            storageComponents.RemoveAt(i);
        }
    }
    StorageSlotComponent CreateSlotComponent(StorageSlotLegacy storageSlot, int id) {
        GameObject go = Instantiate(storageSlotPrefab, UIContainer);
        go.name = "Slot #" + (id + 1);
        StorageSlotComponent storageSlotComponent = go.GetComponent<StorageSlotComponent>();
        storageSlotComponent.SetSlot(storageSlot);
        //AddSlot(storageSlot);
        fill?.SetAsLastSibling();
        return storageSlotComponent;
    }

}
