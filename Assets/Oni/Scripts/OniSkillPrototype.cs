﻿using UnityEngine;
using System;
using System.Collections.Generic;
using UnityEngine.AddressableAssets;

namespace OniNM {
    [CreateAssetMenu(fileName = "NewSkill", menuName = "Oni/Skill/New Skill")]
    public class OniSkillPrototype : ScriptableObject {
        public string Id;
        public OniStance.OniStanceType Type;

        public string Name;
        public string Description;
        public int Range = 1;
        public int Cost = 1;
        public float Cooldown;
        public bool Permanent;
        public int Tier;
        public OniSkillEffectPrototype[] SkillEffects;
        public OniSkilltree Upgrades;
    }
    [Serializable]
    public class OniSkillPrototypeAssetReference : AssetReferenceT<OniSkillPrototype> {
        public OniSkillPrototypeAssetReference(string guid) : base(guid) { }
    }
    [Serializable]
    public class OniSkilltree {
        protected OniSkilltree(OniSkilltree clone) {
            List<SkilltreeItem> branchs = new List<SkilltreeItem>();
            for (int i = 0; i < clone.Branchs.Length; i++) {
                branchs.Add(clone.Branchs[i].Clone());
            }
            Branchs = branchs.ToArray();
        }
        public OniSkilltree Clone() {
            OniSkilltree clone = new OniSkilltree(this);
            return clone;
        }
        public SkilltreeItem[] Branchs;
    }
    [Serializable]
    public class SkilltreeItem {
        public int Cost;
        public OniSkillPrototype Skill;

        protected SkilltreeItem(SkilltreeItem clone) {
            Cost = clone.Cost;
            Skill = clone.Skill;
        }
        public SkilltreeItem Clone() {
            SkilltreeItem clone = new SkilltreeItem(this);
            return clone;
        }
    }

}