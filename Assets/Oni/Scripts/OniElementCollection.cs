﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using OniNM;
public class OniElementCollection : Singleton<OniElementCollection> {
    public override void SingletonAwake() {
        LoadAssetManager.Initialize += LoadAssetManager_Initialize;
        LoadAssetManager.ModuleLoad += Init;
    }
    private IEnumerator LoadAssetManager_Initialize() {
        yield return LoadAssetManager.RequestLoadCollection<OniElementPrototype>("OniElement", elementLoad_Completed);
    }
    void elementLoad_Completed(OniElementPrototype elementPrototype) {
        if (elementPrototype.Id == "physical") {
            _physicalElement = elementPrototype;
        } else {
            elementPrototypes.Add(elementPrototype);
        }
    }
    [SerializeField] List<OniElementPrototype> elementPrototypes = new List<OniElementPrototype>();
    [SerializeField] public OniElementPrototype _physicalElement;
    public static OniElementPrototype PhysicalElement {
        get { return Instance._physicalElement; }
    }

    public static bool GetElement(string elementId, out OniElementPrototype element) {
        element = Instance.elementPrototypes.Find(e => e.Id == elementId);
        return (element != null);
    }
}
