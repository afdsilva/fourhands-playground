﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class tileInfoView : MonoBehaviour {

    public TextMeshProUGUI top_left;
    public TextMeshProUGUI bottom_left;

    public void UpdateNode(OniNM.OniNode node) {
        top_left.text = "g: " + node.G;
        bottom_left.text = "h " + node.H;
    }
    public void Clear() {
        top_left.text = "";
        bottom_left.text = "";
    }
}
