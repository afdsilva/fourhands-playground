﻿using UnityEngine;
using System.Collections.Generic;

namespace OniNM {
    public class OniElement : ISlotable {
        protected OniElement() { }
        protected OniElement(OniElementPrototype prototype) {
            Prototype = prototype;
            Id = prototype.Id;
            Name = prototype.name;
            Description = prototype.Description;
            Tier = prototype.Tier;
            Icon = prototype.Icon;
            _skillPrototypes = (new List<OniSkillPrototype>(prototype.Skills)).ToArray();
        }
        public string Id { get; protected set; }
        public string Name { get; protected set; }
        public string Description { get; protected set; }
        public int Tier { get; protected set; }
        public Sprite Icon { get; protected set; }
        protected OniElementPrototype Prototype;
        protected OniSkillPrototype[] _skillPrototypes;
        Slot _ownerSlot;
        public bool ValidSlot { get { return (_ownerSlot != null); } }

        public void EquipOn(Slot slot) {
            _ownerSlot = slot;
        }

        public Slot GetSlot() {
            return _ownerSlot;
        }
        public OniSkillPrototype[] GetSkillPrototype(OniStance.OniStanceType type, bool useFlag = false) {
            
            List<OniSkillPrototype> list = new List<OniSkillPrototype>(_skillPrototypes);
            if (useFlag) {
                return list.FindAll(s => type.HasFlag(s.Type)).ToArray();
            } else {
                return list.FindAll(s => s.Type == type).ToArray();
            }
        }
        public OniSkillPrototype[] GetSkillPrototype(OniStance.OniStanceType type, int tier, bool useFlag = false) {
            List<OniSkillPrototype> list = new List<OniSkillPrototype>(_skillPrototypes);
            if (useFlag) {
                return list.FindAll(s => type.HasFlag(s.Type) && s.Tier == tier).ToArray();
            } else {
                return list.FindAll(s => s.Type == type && s.Tier == tier).ToArray();
            }
        }

        public static OniElement Instantiate(OniElementPrototype prototype) {
            OniElement element = new OniElement(prototype);
            return element;
        }
    }
}