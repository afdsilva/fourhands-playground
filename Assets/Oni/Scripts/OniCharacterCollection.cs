﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Tilemaps;
using OniNM;

public class OniCharacterCollection : Singleton<OniCharacterCollection> {
    public override void SingletonAwake() {
        LoadAssetManager.ModuleLoad += Init;
        Completed += OniCharacterCollection_Completed;
    }

    Dictionary<OniCharacter, TileCharacterComponent> characterContainer = new Dictionary<OniCharacter, TileCharacterComponent>();

    public GameObject CharacterPrefab;
    public Transform spawnPointA, spawnPointB, spawnPointC, spawnPointD;
    public OniStanceController stanceController;
    OniCharacter selectedOniCharacter;
    OniCharacter playerCharacter;
    public static event System.Action<OniCharacterCollection> OnInitCharactersComplete;

    private IEnumerator OniCharacterCollection_Completed() {
        InitCharacters();
        yield break;
    }
    public void InitCharacters() {
        cbSelectOniCharacter(null);
        Vector3Int SpawnPointACell = WorldToCellCharacter(spawnPointA.position);
        Vector3Int SpawnPointBCell = WorldToCellCharacter(spawnPointB.position);
        OniCharacter.OnClearSelection -= cbClearSelection;
        OniCharacter.OnClearSelection += cbClearSelection;
        OniCharacter.RegisterProviders(WorldToCellCharacter, CellToWorldOffset, GridManagerController.CheckValid, GridManagerController.CheckCost);
        playerCharacter = OniCharacter.Create("Player1", true, spawnPointA.position, OniElementCollection.PhysicalElement);
        //OniSkill.InstanceSkillEffects
        registerCallbacks(playerCharacter);
        cbSelectOniCharacter(playerCharacter);
        //stanceController.UpdateStance(playerCharacter.CurrentStance);
        Spawn(playerCharacter, true);

        //OniCharacter p2Character = new OniCharacter("Player2", false, spawnPointB.position, WorldToCellCharacter, CellToWorldOffset, OniGridManager.CheckValid, OniGridManager.CheckCost);
        OniCharacter p2Character = OniCharacter.Create("Player2", false, spawnPointB.position, OniElementCollection.PhysicalElement);
        //p2Character.SelectSkill(Skill.SkillRock);
        registerCallbacks(p2Character);
        Spawn(p2Character);

        //OniCharacter p3Character = new OniCharacter("Player3", false, spawnPointC.position, WorldToCellCharacter, CellToWorldOffset, OniGridManager.CheckValid, OniGridManager.CheckCost);
        OniCharacter p3Character = OniCharacter.Create("Player3", false, spawnPointC.position, OniElementCollection.PhysicalElement);
        //p3Character.SelectSkill(Skill.SkillPaper);
        registerCallbacks(p3Character);
        Spawn(p3Character);

        //OniCharacter p4Character = new OniCharacter("Player4", false, spawnPointD.position, WorldToCellCharacter, CellToWorldOffset, OniGridManager.CheckValid, OniGridManager.CheckCost);
        OniCharacter p4Character = OniCharacter.Create("Player4", false, spawnPointD.position, OniElementCollection.PhysicalElement);
        //p4Character.SelectSkill(Skill.SkillScissor);
        registerCallbacks(p4Character);
        Spawn(p4Character);

        OnInitCharactersComplete?.Invoke(this);



        //TODO: Test
        //////////////////////////
        //////////////////////////
        //////////////////////////
        //Oni.Instantiate(playerCharacter )
        CombatPhase.OnShowVisuals += CombatPhase_OnShowVisuals;
        playerCharacter.OnShowCombat += PlayerCharacter_OnShowVisuals;
        playerCharacter.OnApplyEffects += PlayerCharacter_OnApplyEffects;
        p2Character.OnShowCombat += PlayerCharacter_OnShowVisuals;
        p2Character.OnApplyEffects += PlayerCharacter_OnApplyEffects;

        playerCharacter.DefaultStance = OniStance.OniStanceType.Paper;
        //Debug.Log(playerCharacter.DefaultStance.ToString() + " x " + p2Character.DefaultStance.ToString()); ;
        playerCharacter.useSkillTest(p2Character);

        //////////////////////////
        //////////////////////////
        //////////////////////////

    }
    private void CombatPhase_OnShowVisuals(OniStance.StanceCondition condition, OniStance.StancePhase phase, System.Action callback) {
        //Debug.Log("CombatPhase_OnShowVisuals " + phase.ToString());
        callback.Invoke();
    }
    List<OniSkillEffect> effectToApply;
    private void PlayerCharacter_OnApplyEffects(OniSkillEffect[] effects, OniStance.StancePhase nextPhase, Combatant character, Combatant enemy, bool isAttacker, System.Action<OniStance.StancePhase, Combatant, Combatant, bool> callback) {
        //Debug.Log("PlayerCharacter_OnApplyEffects " + phase.ToString() + " length: " + effects.Length);
        if (effects.Length <= 0) {
            callback?.Invoke(nextPhase, character, enemy, isAttacker);
            return;
        }
        if (effectToApply == null)
            effectToApply = new List<OniSkillEffect>(effects);
        effectToApply.Clear();
        effectToApply.AddRange(effects);

        for (int i = 0; i < effects.Length; i++) {
            StartCoroutine(fakeApplyVisual(effects[i], nextPhase, character, enemy, isAttacker, callback));
        }
    }
    IEnumerator fakeApplyVisual(OniSkillEffect effect, OniStance.StancePhase nextPhase, Combatant character, Combatant enemy, bool isAttacker, System.Action<OniStance.StancePhase, Combatant, Combatant, bool> callback) {
        Debug.Log("fakeApplyVisual " + character.Name + " effect: " + effect.Id + " nextPhase: " + nextPhase.ToString());
        effectToApply.Remove(effect);
        character.ApplyEffect(effect);
        if (effectToApply.Count <= 0) callback?.Invoke(nextPhase, character, enemy, isAttacker);
        yield break;
    }
    private void PlayerCharacter_OnShowVisuals(OniStance.StanceCondition condition, OniStance.StancePhase phase, Combatant character, Combatant enemy, bool isAttacker, System.Action<OniStance.StanceCondition, OniStance.StancePhase, Combatant, Combatant, bool> callback) {

        StartCoroutine(fakeVisuals(condition, phase, character, enemy, isAttacker, callback));
    }
    IEnumerator fakeVisuals(OniStance.StanceCondition condition, OniStance.StancePhase phase, Combatant character, Combatant enemy, bool isAttacker, System.Action<OniStance.StanceCondition, OniStance.StancePhase, Combatant, Combatant, bool> callback) {
        float rand = Random.Range(2, 5);
        //Debug.Log("fakeVisuals " + rand + " phase: " + phase.ToString() + " character " + character.Name);
        
        yield return new WaitForSeconds(rand);
        callback?.Invoke(condition, phase, character, enemy, isAttacker);

        yield break;
    }

    Vector3Int WorldToCellCharacter(Vector3 pos) {
        GridManagerController.defaultTilemapType = GridManagerController.TilemapType.Character;
        return GridManagerController.WorldToCell(pos);
    }
    Vector3 CellToWorldOffset(Vector3Int tilePos) {
        GridManagerController.defaultTilemapType = GridManagerController.TilemapType.Character;
        return GridManagerController.CellToWorldOffset(tilePos);
    }
    public void Spawn(OniCharacter characterId, bool isPlayer = false) {
        GameObject go = Instantiate(CharacterPrefab, this.transform);
        TileCharacterComponent tcc = go.GetComponent<TileCharacterComponent>();
        go.name = characterId.Name;
        tcc.SetCharacter(characterId);
        characterContainer[characterId] = tcc;
        if (isPlayer) {
            tcc.show();
        } else {
            tcc.hide();
        }
    }
    public void registerCallbacks(OniCharacter oniCharacter) {
        unregisterCallbacks(oniCharacter);
        TileCharacter.OnRequestCharacterInfo += oniCharacter.CheckPosition;
        //oniCharacter.OnSelection += cbSelectOniCharacter;
        //oniCharacter.OnSelection += StorageSlotManager.Instance.SetCharacter;
        oniCharacter.registerSightProviders(GridManagerController.Instance.cbOnProviderCharsInRange, GridManagerController.Instance.cbOnProviderCheckSight);
        //oniCharacter.providerSightedCharacters += OniGridManager.Instance.cbOnProviderSightedChars;
        //oniCharacter.providerCheckSight += OniGridManager.Instance.cbOnProviderCheckSight;
        //oniCharacter.OnStanceChange += stanceController.UpdateStance; //stance is registerd on OniStanceController
    }
    public void unregisterCallbacks(OniCharacter oniCharacter) {
        TileCharacter.OnRequestCharacterInfo -= oniCharacter.CheckPosition;
        //oniCharacter.OnSelection -= cbSelectOniCharacter;
        //oniCharacter.OnSelection -= StorageSlotManager.Instance.SetCharacter;
        //oniCharacter.OnStanceChange -= stanceController.UpdateStance;
    }
    void cbSelectOniCharacter(OniCharacter oniCharacter) {
        //Debug.Log(GetType().FullName + ".cbSelectOniCharacter = " + oniCharacter?.ToString());
        selectedOniCharacter = oniCharacter;
    }
    bool cbClearSelection(OniCharacter oniCharacter) {
        bool clearedSelf = false;
        if (oniCharacter != null && selectedOniCharacter == oniCharacter) clearedSelf = true;
        selectedOniCharacter = null;
        //Debug.Log(GetType().FullName + ".cbClearSelection = " + oniCharacter?.ToString() + " clearedSelf: " + clearedSelf);
        return clearedSelf;
    }
    public static bool GetCharacter(OniCharacter characterId, out TileCharacterComponent characterComponent) {
        return Instance.characterContainer.TryGetValue(characterId, out characterComponent);
    }
    public static bool GetSelectedCharacter(out OniCharacter oniCharacter) {
        oniCharacter = Instance.selectedOniCharacter;
        return (oniCharacter != null);
    }
    public static bool GetPlayerCharacter(out OniCharacter playerCharacter) {
        playerCharacter = Instance.playerCharacter;
        return (playerCharacter != null);
    }
    public static bool CheckSelectedPlayerCharacter(OniCharacter oniCharacter) {
        return (Instance.playerCharacter == oniCharacter);
    }
    public static bool CheckSelectedCharacter(OniCharacter oniCharacter) {
        return (Instance.selectedOniCharacter == oniCharacter);
    }
}
