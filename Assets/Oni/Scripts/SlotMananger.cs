﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

namespace OniNM {
    public interface ISlotList {
        bool AddSlot<T>(T slot) where T : Slot;
        bool GetEmptySlot<T>(out T slot) where T : Slot;
        bool GetSlot<T>(int index, out T slot) where T : Slot;
        T[] GetSlots<T>() where T : Slot;
        bool RemoveSlot<T>(T slot) where T : Slot;
    }

    public abstract class SlotMananger {
        private SlotMananger() { }
        protected SlotMananger(Combatant owner) {
            this.owner = owner;
        }
        Combatant owner;
        public abstract bool GetSlot(int index, out Slot slot);
        public abstract int GetIndex(Slot slot);
        public abstract bool GetContent<T>(int index, out T content) where T : ISlotable;
        public Combatant GetOwner() {
            return owner;
        }
    }
    public abstract class SlotMananger<T> : SlotMananger where T : ISlotable {
        protected SlotMananger(Combatant owner) : base(owner) {

        }

        public abstract bool GetContent(int index, out T content);
    }

    public class SkillManager : SlotMananger<OniSkill> {
        protected SkillManager(Combatant owner) : base(owner) {
            knowSkills = new List<OniSkill>();
            rockSkillSlot = SkillSlot.Create(this);
            paperSkillSlot = SkillSlot.Create(this);
            scissorSkillSlot = SkillSlot.Create(this);
        }
        List<OniSkill> knowSkills;
        protected SkillSlot rockSkillSlot;
        protected SkillSlot paperSkillSlot;
        protected SkillSlot scissorSkillSlot;

        public void LearnSkill(OniSkillPrototype skillPrototype) {
            //Debug.Log("LearnSkill: " + skillPrototype.Name);
            OniSkill found = knowSkills.Find(s => s.Id == skillPrototype.Id);
            if (found == null) {
                OniSkill skill = OniSkill.Instantiate(skillPrototype, GetOwner());
                knowSkills.Add(skill);
                SkillSlot slot;
                if (GetSlot(skill.Type, out slot) == true) {
                    if (slot.IsEmpty) slot.Equip(skill);
                }
            }
        }

        public bool ForgetSkill(string id, out OniSkill forgotSkill) {
            forgotSkill = knowSkills.Find(s => s.Id == id);
            if (forgotSkill == null)
                return false;
            if (knowSkills.Remove(forgotSkill) == false)
                return false;
            SkillSlot slot;
            if (GetSlot(forgotSkill.Type, out slot) == true) {
                if (slot.Contains(forgotSkill) == true) return slot.Remove(out forgotSkill);
            }
            return true;
        }
        public bool ForgetSkill(string id) {
            OniSkill forgotSkill;
            return ForgetSkill(id, out forgotSkill);
        }
        public bool ForgetSkill(OniSkill forgotSkill) {
            return knowSkills.Remove(forgotSkill);
        }
        public OniSkill[] GetKnowSkills() {
            return knowSkills.ToArray();
        }
        public bool GetSkill(OniStance.OniStanceType stanceType, out OniSkill[] skills) {
            skills = knowSkills.FindAll(s => s.Type == stanceType).ToArray();
            return true;
        }
        public bool GetKnowSkill(int index, out OniSkill skill) {
            if (index < 0 || index >= knowSkills.Count) {
                skill = null;
                return false;
            }
            skill = knowSkills[index];
            return true;
        }
        public bool GetKnowSkill(string id, out OniSkill skill) {
            skill = knowSkills.Find(s => s.Id == id);
            if (skill == null) {
                return false;
            }
            return true;
        }
        public bool GetSkill(OniStance.OniStanceType stanceType, out OniSkill skill) {
            SkillSlot skillSlot;
            if (GetSlot(stanceType, out skillSlot) == false) throw new Exception("slot not found for stance " + stanceType.ToString());
            return skillSlot.GetContent(out skill);
        }
        public bool GetSlot(OniStance.OniStanceType stanceType, out SkillSlot slot) {
            slot = null;
            switch (stanceType) {
                case OniStance.OniStanceType.Rock:
                    Slot s;
                    if (GetSlot(0, out s) == false) {
                        return false;
                    }
                    slot = s as SkillSlot;
                    break;
                case OniStance.OniStanceType.Paper:
                    if (GetSlot(1, out s) == false) {
                        return false;
                    }
                    slot = s as SkillSlot;
                    break;
                case OniStance.OniStanceType.Scissor:
                    if (GetSlot(2, out s) == false) {
                        return false;
                    }
                    slot = s as SkillSlot;
                    break;
            }
            return (slot != null);
        }
        public override bool GetSlot(int index, out Slot slot) {
            slot = null;
            switch (index) {
                case 0:
                    slot = rockSkillSlot;
                    break;
                case 1:
                    slot = paperSkillSlot;
                    break;
                case 2:
                    slot = scissorSkillSlot;
                    break;
            }
            return (slot != null);
        }
        public override int GetIndex(Slot slot) {
            if (slot == rockSkillSlot) return 0;
            else if (slot == paperSkillSlot) return 1;
            else if (slot == scissorSkillSlot) return 2;
            else return -1;
        }
        public bool UseSlot(OniStance.OniStanceType stanceType, int apAvailable) {
            SkillSlot slot;
            if (GetSlot(stanceType, out slot) == false) return false;
            Exception e;
            return slot.Use(apAvailable, out e);
        }
        public override bool GetContent(int index, out OniSkill content) {
            switch (index) {
                case 0:
                    return rockSkillSlot.GetContent(out content);
                case 1:
                    return paperSkillSlot.GetContent(out content);
                case 2:
                    return scissorSkillSlot.GetContent(out content);
            }
            content = null;
            return false;
        }
        public override bool GetContent<T>(int index, out T content) {
            if (typeof(T) != typeof(OniSkill)) {
                throw new NotImplementedException(GetType().FullName + "::" + typeof(T).FullName + " not valid type.");
            }
            OniSkill oniSkill;
            if (GetContent(index, out oniSkill) == true) {
                content = (T)(oniSkill as ISlotable);
                return true;
            }
            content = default;
            return false;
        }
        public static SkillManager Create(Combatant owner) {
            SkillManager manager = new SkillManager(owner);
            //Debug.Log("SkillManager of " + manager.owner.Name);
            return manager;
        }

    }
    public class OniManager : SlotMananger<Oni> {
        List<Oni> log;
        protected OniManager(Combatant owner) : base (owner) {
            log = new List<Oni>();
            oniSlot = OniSlot.Create(this);
        }
        protected OniSlot oniSlot;

        public bool Equip(Oni oni) {
            //Debug.Log(GetType().FullName + "::Equip::" + oni.ToString());
            if (oniSlot.Equip(oni) == false) {
                return false;
            }

            foreach (var item in oni.GetSkills()) {
                //Debug.Log("Learning: " + item.Name);
                //OniSkill skill = OniSkill.Instantiate(item, owner);
                GetOwner().SkillManager.LearnSkill(item);
            }
            //owner.SkillManager.LearnSkill()
            return true;
        }
        public bool Remove(out Oni oni) {
            if (oniSlot.Remove(out oni) == false) return false;
            log.Add(oni);
            foreach (var item in oni.GetSkills()) {
                if (item.Permanent == false) { //remove apenas skills que nao sao permanentes
                    GetOwner().SkillManager.ForgetSkill(item.Id);
                }
            }
            return true;
        }
        public override bool GetSlot(int index, out Slot slot) {
            slot = null;
            switch (index) {
                case 0:
                    slot = oniSlot;
                    break;
            }
            return (slot != null);
        }
        public override int GetIndex(Slot slot) {
            if (slot == oniSlot) return 0;
            else return -1;
        }
        public override bool GetContent(int index, out Oni content) {
            Slot s;
            content = null;
            if (GetSlot(index, out s) == false) return false;
            return s.GetContent(out content);
        }
        public override bool GetContent<T>(int index, out T content) {
            if (typeof(T) != typeof(Oni)) {
                throw new NotImplementedException(GetType().FullName + "::" + typeof(T).FullName + " not valid type.");
            }
            Oni oni;
            if (GetContent(index, out oni) == true) {
                content = (T)(oni as ISlotable);
                return true;
            }
            content = default;
            return false;
        }

        public static OniManager Create(Combatant owner) {
            OniManager manager = new OniManager(owner);
            return manager;
        }

    }
}