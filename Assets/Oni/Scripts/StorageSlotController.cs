﻿using UnityEngine;
using OniNM;

[RequireComponent(typeof(StorageSlotComponent))]
public class StorageSlotController : MonoBehaviour {
    private void Awake() {
        StorageSlotComponent = GetComponent<StorageSlotComponent>();
    }
    public StorageSlotComponent StorageSlotComponent;
    public StorageSlotLegacy StorageSlot { get { return StorageSlotComponent.StorageSlot; } }
    //public void Equip(Oni oni) {
    //    if (StorageSlot.IsEmpty == false) return;
    //    if (StorageSlot.Equip(oni)) {
    //        //success
    //    } else {
    //        //failure
    //    }
    //}
    //public bool Unequip(out Oni oni) {
    //    oni = null;
    //    if (StorageSlot.Unequip(out oni)) {
    //        //success
    //        return true;
    //    } else {

    //        //failure
    //        return false;
    //    }
    //}
}
