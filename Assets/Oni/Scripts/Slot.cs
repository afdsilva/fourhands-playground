﻿using UnityEngine;
using System;
using System.Collections.Generic;

namespace OniNM {
    public interface ISlotable {
        void EquipOn(Slot slot);
        Slot GetSlot();
        bool ValidSlot { get; }
    }

    public class SlotException : Exception {
        public SlotException(string message) : base(message) { }
    }
    public abstract class Slot {
        public virtual bool IsEmpty { get; }

        public abstract bool Equip<T>(T other) where T : ISlotable;
        public abstract bool Remove<T>(out T other) where T : ISlotable;
        public abstract bool GetContent<T>(out T content) where T : ISlotable;
        public abstract bool Contains<T>(T content) where T : ISlotable;
        public abstract void Update(float deltaTime);
        public abstract void SwitchSlot<T>(Slot<T> other) where T : ISlotable;
        public abstract void ManagedBy(SlotMananger manager);
        public abstract bool GetManager(out SlotMananger manager);
    }
    public abstract class Slot<T> : Slot where T : ISlotable {
        public virtual event System.Action<T> OnSlotChange;
        public abstract bool Equip(T other);
        public abstract bool Remove(out T other);
        public abstract bool GetContent(out T content);
        public abstract bool Contains(T content);
        public abstract void SwitchSlot(Slot<T> other);
    }
    public class OniSlot : Slot<Oni> {
        protected OniSlot() {
            //log = new List<Oni>();
        }

        Oni oni;
        SlotMananger manager;

        #region Slot Implementation
        [field: NonSerialized] public override event System.Action<Oni> OnSlotChange;
        public override bool IsEmpty { get { return (oni == null); } }
        public override bool Equip(Oni other) {
            if (!IsEmpty || other == null) {
                return false;
            }
            oni = other;
            oni.EquipOn(this);
            OnSlotChange?.Invoke(oni);
            
            //Debug.Log("Equipped " + oni.ToString() + " on OniSlot for " + manager.GetOwner().Name);
            return true;
        }
        public override bool Equip<T>(T other) {
            if (typeof(T) != typeof(Oni)) throw new NotImplementedException(GetType().FullName + "::Equip::Type " + typeof(T).FullName + " not implement.");
            return Equip(other as Oni);
        }
        public override bool Remove(out Oni other) {
            other = oni;
            if (other == null) {
                return false;
            }
            //log.Add(other);
            oni = null;
            OnSlotChange?.Invoke(null);
            return true;
        }
        public override bool Remove<T>(out T other) {
            if (typeof(T) != typeof(Oni)) throw new NotImplementedException(GetType().FullName + "::Remove::Type " + typeof(T).FullName + " not implement.");

            Oni removedOni;
            if (Remove(out removedOni) == true) {
                other = (T)(removedOni as ISlotable);
                if (other == null) {//CLEANUP: NOT NEEDED IF WORKS
                    throw new Exception("other should never be null, probably this dosn't work.");
                }
                return true;
            }
            other = default;
            return false;
        }

        public override bool GetContent(out Oni content) {
            content = oni;
            return (content != null);
        }
        public override bool GetContent<T>(out T content) {
            if (typeof(T) != typeof(Oni)) throw new Exception("Can't get a Non-Oni from this slot.");
            Oni oni;
            if (GetContent(out oni) == false) {
                ISlotable mySlotable = oni;
                content = (T)mySlotable;
                if (content == null) {//CLEANUP: NOT NEEDED IF WORKS
                    throw new Exception("other should never be null, probably this dosn't work.");
                }
                return true;
            }
            content = default;
            return false;
        }
        public override bool Contains<T>(T content) {
            if (typeof(T) != typeof(Oni)) throw new NotImplementedException(GetType().FullName + "::Contains::Type " + typeof(T).FullName + " not implement.");
            return (oni == (content as Oni));
        }
        public override bool Contains(Oni content) {
            return (oni == content);
        }
        public override void Update(float deltaTime) {
            oni?.Update(deltaTime);
        }
        public override void SwitchSlot<T>(Slot<T> other) {
            if (typeof(T) != typeof(Oni)) throw new SlotException("OniSlot can only exchange with other OniSlot.");
            SwitchSlot(other as OniSlot);
        }
        public override void SwitchSlot(Slot<Oni> other) {
            //nao importa o conteudo de cada slot
            //pois mesmo se for dois slots vazios as operacoes vao ser canceladas internamente por cada Equip()
            other.Remove(out Oni otherOni);
            Remove(out Oni myOni);
            other.Equip(myOni);
            Equip(otherOni);
        }
        public override void ManagedBy(SlotMananger manager) {
            this.manager = manager;
        }
        public override bool GetManager(out SlotMananger manager) {
            manager = this.manager;
            return (manager != null);
        }
        #endregion

        public static OniSlot Create(SlotMananger manager, Oni oni = null) {
            OniSlot slot = new OniSlot();
            //slot.ManagedBy(source);
            slot.ManagedBy(manager);
            slot.Equip(oni);
            return slot;
        }

    }
    public class SkillSlot : Slot<OniSkill> {
        protected SkillSlot() { }

        SlotMananger manager;
        OniSkill skill;
        public bool Use(int apAvailable, out Exception e) {
            if (skill == null) {
                e = new NullReferenceException();
                return false;
            }
            return skill.Use(apAvailable, out e);
        }

        #region Slot Implementation
        public override event System.Action<OniSkill> OnSlotChange;
        public override bool IsEmpty { get { return (skill == null); } }
        public override bool Equip(OniSkill other) {
            if (other == null) return false;
            skill = other;
            skill.EquipOn(this);
            OnSlotChange?.Invoke(skill);
            return true;
        }
        public override bool Equip<T>(T other) {
            if (other.GetType() != typeof(OniSkill)) return false;
            OniSkill oni = other as OniSkill;
            return Equip(other);
        }
        public override bool Remove(out OniSkill other) {
            other = skill;
            if (other == null) {
                return false;
            }
            skill = null;
            OnSlotChange?.Invoke(null);
            return true;
        }
        public override bool Remove<T>(out T other) {
            other = default;
            if (typeof(T) != typeof(OniSkill)) return false;
            OniSkill removedSkill;
            if (Remove(out removedSkill) == false) {
                other = default;
                return false;
            }
            ISlotable mySlotable = removedSkill;
            other = (T)mySlotable;
            if (other == null) {
                throw new Exception("other should never be null, probably this dosn't work.");
            }
            return true;
        }
        public override bool GetContent(out OniSkill content) {
            content = skill;
            return (content != null);
        }
        public override bool GetContent<T>(out T content) {
            content = default;
            if (typeof(T) != typeof(OniSkill)) return false;
            OniSkill skill;
            if (GetContent(out skill) == false) {
                content = default;
                return false;
            }
            ISlotable mySlotable = skill;
            content = (T)mySlotable;
            if (content == null) {
                throw new Exception("other should never be null, probably this dosn't work.");
            }
            return true;
        }
        public override bool Contains<T>(T content) {
            if (typeof(T) != typeof(OniSkill)) throw new NotImplementedException(GetType().FullName + "::Contains::Type " + typeof(T).FullName + " not implement.");
            return (skill == (content as OniSkill));
        }
        public override bool Contains(OniSkill content) {
            return (skill == content);
        }
        public override void Update(float deltaTime) {
            skill?.Update(deltaTime);
        }
        public override void SwitchSlot<T>(Slot<T> other) {
            if (typeof(T) != typeof(OniSkill)) throw new SlotException("SkillSlot can only exchange with other SkillSlot.");
            SkillSlot otherSkillSlot = other as SkillSlot;
            SwitchSlot(otherSkillSlot);
        }
        public override void SwitchSlot(Slot<OniSkill> other) {
            OniSkill otherSkill;
            other.Remove(out otherSkill);
            OniSkill mySkill;
            Remove(out mySkill);

            other.Equip(mySkill);
            Equip(otherSkill);

        }
        public override void ManagedBy(SlotMananger manager) {
            this.manager = manager;
        }
        public override bool GetManager(out SlotMananger manager) {
            manager = this.manager;
            return (manager != null);
        }
        #endregion

        public static SkillSlot Create(SlotMananger manager, OniSkill skill = null) {
            SkillSlot skillSlot = new SkillSlot();
            skillSlot.ManagedBy(manager);
            skillSlot.Equip(skill);
            return skillSlot;
        }
    }
    public class StorageSlot : Slot {
        SlotMananger manager;
        object content;
        public override bool IsEmpty { get { return (content == null); } }

        public override bool Equip<T>(T other) {
            if (content != null) return false;
            content = other;
            other.EquipOn(this);
            return true;
        }
        public override bool GetContent<T>(out T content) {
            if (this.content == null) {
                content = default;
                return false;
            }
            if (this.content.GetType() != typeof(T)) throw new Exception("wrong type");
            content = (T)this.content;
            return (content != null);
        }
        public override bool Contains<T>(T content) {
            if (typeof(T) != typeof(ISlotable)) throw new NotImplementedException(GetType().FullName + "::Contains::Type " + typeof(T).FullName + " not implement.");
            return (this.content == (content as ISlotable));
        }
        public override bool Remove<T>(out T other) {
            if (content == null) {
                other = default;
                return false;
            }
            if (content.GetType() != typeof(T)) throw new Exception("wrong type");
            other = (T)content;
            if (other == null) return false;
            content = null;
            return true;
        }
        public override void SwitchSlot<T>(Slot<T> other) {
            ISlotable mySlotable;
            ISlotable otherSlotable;
            other.Remove(out otherSlotable);
            Remove(out mySlotable);

            Equip(otherSlotable);
            other.Equip(mySlotable);
        }
        public override void Update(float deltaTime) { }
        public override void ManagedBy(SlotMananger manager) {
            this.manager = manager;
        }
        public override bool GetManager(out SlotMananger manager) {
            manager = this.manager;
            return (manager != null);
        }

        public static StorageSlot Create(SlotMananger manager) {
            StorageSlot storageSlot = new StorageSlot();
            storageSlot.manager = manager;
            return storageSlot;
        }
    }

    #region SLOT LEGACY
    [Obsolete("SlotLegacy will not be used anymore")]
    public abstract class SlotLegacy {
        public static SlotLegacy SlotTransaction;
        public virtual event System.Action OnSlotChange;
        protected SlotLegacy() {
        }
        protected ISlotable slotable;
        public bool IsEmpty { get { return (slotable == null); } }

        public virtual bool Equip(ISlotable other) {
            if (other == null) return false;
            slotable = other;
            OnSlotChange?.Invoke();
            return true;
        }
        public virtual bool Unequip(out ISlotable other) {
            if (slotable == null) {
                other = null;
                return false;
            }
            other = slotable;
            slotable = null;
            OnSlotChange?.Invoke();
            return (true);
        }
        public virtual bool SwitchSlot(SlotLegacy b) {
            ISlotable mySlotable;
            ISlotable otherSlotable;

            if (Unequip(out mySlotable) == true) {//pressupoe que o Slot atual possui Oni e quer trocar com 'b'
                if (b.Equip(mySlotable) == false) {
                    //se nao conseguir equipar
                    if (b.Unequip(out otherSlotable) == false) //Slot 'b' nesse caso sempre deve ser desequipado
                        throw new Exception("Slot " + b.ToString() + " nao foi possivel desequipar o slot.");
                    if (Equip(otherSlotable) == false) //agora eh possivel equipar o oni
                        throw new Exception("Slot " + ToString() + " nao foi possivel equipar o slot.");
                    //nao eh necessario equipar 'myOni' pois ele 
                    if (b.Equip(mySlotable) == false)
                        throw new Exception("Slot " + b.ToString() + " nao foi possivel equipar o slot.");
                }
                //se chegou ate aqui entao a operacao foi bem sucedida
                return true;
            }
            if (b.IsEmpty) return false;
            return (b.SwitchSlot(this));
        }
    }

    [Obsolete("StorageSlotLegacy will not be used anymore")]
    public class StorageSlotLegacy : SlotLegacy {
        
        public StorageSlotLegacy() {
        }
        public override event System.Action OnSlotChange;

        public bool GetContent(out ISlotable slotable) {
            slotable = base.slotable;
            return (slotable != null);
        }

        public override bool Equip(ISlotable slotable) {
            if (slotable == null) return false;
            base.slotable = slotable;
            OnSlotChange?.Invoke();
            return true;
        }
        public override bool Unequip(out ISlotable slotable) {
            if (base.slotable == null) {
                slotable = null;
                return false;
            }
            slotable = base.slotable;
            base.slotable = null;
            OnSlotChange?.Invoke();
            return true;
        }
    }

    #endregion
}