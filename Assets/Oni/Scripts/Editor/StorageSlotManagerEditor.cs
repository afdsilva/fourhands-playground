﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
[CustomEditor(typeof(StorageSlotManager))]
public class StorageSlotManagerEditor : Editor {
    public StorageSlotManager manager;
    private void OnEnable() {
        manager = (StorageSlotManager)target;
    }
    public override void OnInspectorGUI() {
        base.OnInspectorGUI();
        //if (GUILayout.Button("Add Storage Slot") == true) {
        //    StorageSlotController controller;
        //    if (CreateStorageSlotController(out controller) == false) {
        //        Debug.LogError("Nao foi possivel criar storage slot.");
        //    }
        //}
    }

    public bool CreateStorageSlotController(out StorageSlotController controller) {
        GameObject sscGO = Instantiate(manager.storageSlotPrefab, manager.transform);
        manager.fill?.SetAsLastSibling();
        controller = sscGO.GetComponent<StorageSlotController>();
        int count = manager.transform.childCount - 1;
        controller.name = "Slot " + count;
        //manager.AddSlot(controller.StorageSlot);
        //SlotControllers.Add(controller);
        //Slots.Add(controller.GetStorageSlot());
        return true;
    }
}
