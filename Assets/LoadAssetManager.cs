﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class LoadAssetManager : Singleton<LoadAssetManager> {
    public override void SingletonAwake() {

    }
    private IEnumerator Start() {
        yield return Init();

        if (ModuleLoad != null) {
            foreach (System.Func<IEnumerator> item in ModuleLoad.GetInvocationList()) {
                yield return item.Invoke();
            }
        }


    }
    [field: System.NonSerialized] public static event System.Func<IEnumerator> ModuleLoad;

    public TextMeshProUGUI assetLabel;
    public TextMeshProUGUI assetPercentage;
    public RectTransform loadPanel;

    List<AddressableOperation> addressableLoadOperations = new List<AddressableOperation>();
    public static RequestAddressableAsset<T> RequestLoadAsset<T>(string location) {
        RequestAddressableAsset<T> operation = Instance.addressableLoadOperations.Find(op => op.Location == location) as RequestAddressableAsset<T>;
        if (operation == null) {
            operation = new RequestAddressableAsset<T>(location);
            Instance.addressableLoadOperations.Add(operation);
        }
        operation.OnUpdateProgress += Instance.Operation_OnUpdateProgress;
        return operation;
    }
    protected void Operation_OnUpdateProgress(string label, float value) {
        assetLabel.text = label;
        assetPercentage.text = value + "%";
    }

    public static RequestAddressableCollection<T> RequestLoadCollection<T>(string location, Action<T> callback) {
        RequestAddressableCollection<T> operation = Instance.addressableLoadOperations.Find(op => op.Location == location) as RequestAddressableCollection<T>;
        if (operation == null) {
            operation = new RequestAddressableCollection<T>(location, callback);
            Instance.addressableLoadOperations.Add(operation);
        }
        return operation;
    }

    void Update() {

        for (int i = 0; i < addressableLoadOperations.Count;) {
            var op = addressableLoadOperations[i];

            if (op.Update()) {
                
                i++;
            }
            else addressableLoadOperations.RemoveAt(i);
        }
    }

}
